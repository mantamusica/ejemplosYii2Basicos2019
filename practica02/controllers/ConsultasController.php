<?php

namespace app\controllers;

use Yii;
use app\models\Consultas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Emple;
use app\models\Depart;

/**
 * ConsultasController implements the CRUD actions for Consultas model.
 */
class ConsultasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Consultas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Consultas::find(),
            'pagination' => [
                'pageSize' =>10
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Consultas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Consultas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Consultas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Consultas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Consultas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Consultas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Consultas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Consultas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionConsulta_1(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find();

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_1",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 01',
            'texto' => 'Mostrar todos los campos y todos los registros de la tabla empleado.',
        ]);

    }
    public function actionConsulta_2(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $datos = Depart::find();

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$datos,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_2",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 02',
            'texto' => 'Mostrar todos los campos y todos los registros de la tabla departamento.',
        ]);

    }
    public function actionConsulta_3(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->select('apellido, oficio');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_3",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 03',
            'texto' => 'Mostrar el apellido y oficio de cada empleado.',
        ]);

    }
    public function actionConsulta_4(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $datos = Depart::find();

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$datos,
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);

        return $this->render("consultas/consulta_4",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 04',
            'texto' => 'Mostrar la localización y número de cada departamento.',
        ]);

    }
    public function actionConsulta_5(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $datos = Depart::find();

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$datos,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_5",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 05',
            'texto' => 'Mostrar el número, nombre y localización de cada departamento.',
        ]);

    }
    public function actionConsulta_6(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->select('count(*) AS salida');
        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
        ]);

        return $this->render("consultas/consulta_6",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 06',
            'texto' => 'Indicar el número de empleados que hay.',
        ]);

    }
    public function actionConsulta_7(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->orderBy(['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_7",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 07',
            'texto' => 'Datos de los empleados ordenados por apellido de forma ascendente.',
        ]);

    }
    public function actionConsulta_8(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->orderBy(['apellido' => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_8",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 08',
            'texto' => 'Datos de los empleados ordenados por apellido de forma descendente.',
        ]);

    }
    public function actionConsulta_9(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Depart::find()->select('count(*) AS salida');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
        ]);

        return $this->render("consultas/consulta_9",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 09',
            'texto' => 'Indicar el número de departamentos que hay.',

        ]);
    }
    public function actionConsulta_10(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->orderBy(['dept_no' => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_10",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 10',
            'texto' => 'Datos de los empleados ordenados por número departamento descendentemente.',
        ]);

    }
    public function actionConsulta_11(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->orderBy(['dept_no' => SORT_DESC], ['oficio' => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_11",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 11',
            'texto' => 'Datos de los empleados ordenados por num. depart. descendentemente y por oficio ascendente.',
        ]);

    }
    public function actionConsulta_12(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->orderBy(['dept_no' => SORT_DESC], ['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_12",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 12',
            'texto' => 'Datos de los empleados ordenados por num. depart. descendentemente y por apellido ascendente.',
        ]);

    }
    public function actionConsulta_13(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->where('salario > 2000');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_13",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 13',
            'texto' => 'Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.',
        ]);

    }
    public function actionConsulta_14(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->select('emp_no, apellido')
            ->where('salario < 2000');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_14",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 14',
            'texto' => 'Mostrar los códigos y los apellidos de los empleados cuyo salario sea menor que 2000.',
        ]);

    }
    public function actionConsulta_15(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->where(['between', 'salario', 1500, 2500]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_15",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 15',
            'texto' => 'Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500.',
        ]);

    }
    public function actionConsulta_16(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->where(['like','oficio','ANALISTA']);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_16",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 16',
            'texto' => 'Mostrar los datos de los empleados cuyo oficio sea ANALISTA.',
        ]);

    }
    public function actionConsulta_17(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()->where(
            ['and',
                'salario>2000',
                ['like','oficio','ANALISTA']
            ]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_17",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 17',
            'texto' => 'Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen más de 2000.',
        ]);

    }
    public function actionConsulta_18(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->select('apellido, oficio')
            ->where(['dept_no' => 20]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_18",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 18',
            'texto' => 'Seleccionar el apellido y oficio de los empleados del departamento número 20.',
        ]);

    }
    public function actionConsulta_19(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->select('COUNT(*) as salida')
            ->where(['like','oficio','VENDEDOR']);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_19",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 19',
            'texto' => 'Contar el número de empleados cuyo oficio sea VENDEDOR.',
        ]);

    }
    public function actionConsulta_20(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->where([
                'OR',
                'left(apellido,1)="n"',
                'left(apellido,1)="m"',
            ])
            ->orderBy(['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_20",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 20',
            'texto' => 'Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellidos ascendentemente.',
        ]);

    }
    public function actionConsulta_21(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r=Emple::find()
            ->where(['oficio' => 'VENDEDOR'])
            ->orderBy(['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_21",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 21',
            'texto' => 'Seleccionar los empleados cuyo oficio sea VENDEDOR ordenados por apellido de forma ascendente.',
        ]);

    }
    public function actionConsulta_22(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $max = Emple::find()
            ->max('salario');


        $r=Emple::find()
            ->select('apellido')
            ->where('salario =' . (integer)$max);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_22",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 22',
            'texto' => 'Mostrar los apellidos del empleado que más gana.',
        ]);

    }
    public function actionConsulta_23(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->where(
                ['and',
                    ['like','oficio','ANALISTA'],
                    ['like','dept_no',10]
                ])
            ->orderBy(['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_23",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 23',
            'texto' => 'Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA ordenando por apellido y oficio ascendentemente.',
        ]);

    }
    public function actionConsulta_24(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        //Creamos una variable en el modelo para guardar la fecha $salida
        $r=Emple::find()
            ->select('month(fecha_alt) as salida')
            ->distinct('salida')
            ->orderBy('salida');

        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);


        return $this->render("consultas/consulta_24",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 24',
            'texto' => 'Listado de los distintos meses en que los empleados se han dado de alta.',
        ]);

    }
    public function actionConsulta_25(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        //Creamos una variable en el modelo para guardar la fecha $salida
        $r=Emple::find()
            ->select('year(fecha_alt) as salida')
            ->distinct('salida')
            ->orderBy('salida');

        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);


        return $this->render("consultas/consulta_25",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 25',
            'texto' => 'Listado de los distintos años en que los empleados se han dado de alta.',
        ]);

    }
    public function actionConsulta_26()
    {
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        //Creamos una variable en el modelo para guardar la fecha $salida
        $r=Emple::find()
            ->select('day(fecha_alt) as salida')
            ->distinct('salida')
            ->orderBy('salida');


        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);


        return $this->render("consultas/consulta_26",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 26 - ',
            'texto' => 'Listado de los distintos días en que los empleados se han dado de alta.',
        ]);

    }
    public function actionConsulta_27(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido')
            ->where([
                'or',
                ['=','dept_no',20],
                ['>','salario',2000]
            ]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_27",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 27',
            'texto' => 'Mostrar los apellidos de los empleados que tengan un salario mayor de 2000 o que pertenezcan al departamento número 20.',
        ]);

    }
    public function actionConsulta_28(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido, dept_no');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_28",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 28',
            'texto' => 'Listado que nos coloque el apellido del empleado y el nombre del departamento al que pertenece.',
        ]);

    }
    public function actionConsulta_29(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido, oficio, dept_no')
            ->orderBy(['apellido' => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_29",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 29',
            'texto' => 'Listado del apellido, el oficio y el nombre del departamento
                        al que pertenece el empleado ordenándolos por apellido de forma descendente.',
        ]);

    }
    public function actionConsulta_30(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select(['dept_no, COUNT(*) AS salida'])
            ->groupBy('dept_no');



        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],

        ]);

        return $this->render("consultas/consulta_30",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 30',
            'texto' => 'Listar el número de empleados por departamento.',
        ]);

    }
    public function actionConsulta_31(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select(['dept_no, COUNT(*) AS salida'])
            ->groupBy('dept_no');



        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_31",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 31',
            'texto' => 'Listar el número de empleados por departamento con nombre departamento.',
        ]);

    }
    public function actionConsulta_32(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido')
            ->orderBy(['oficio' => SORT_DESC], ['nombre' => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_32",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 32',
            'texto' => 'Listar el apellido de todos los empleados y ordenadorlos por oficio, y por nombre.',
        ]);

    }
    public function actionConsulta_33(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido')
            ->where('left(apellido,1)="a"');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_34",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 33',
            'texto' => 'Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados.',
        ]);

    }
    public function actionConsulta_34(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()
            ->select('apellido')
            ->where([
                'OR',
                'left(apellido,1)="a"',
                'left(apellido,1)="m"',
            ])
            ->orderBy(['apellido' => SORT_ASC]);

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_34",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 34',
            'texto' => 'Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados.',
        ]);

    }
    public function actionConsulta_35(){
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */

        $r=Emple::find()->where('right(apellido,1)<>"z"');

        /**
         * voy a utilizar un dataprovider
         * desde activeQuery
         */
        $d=new ActiveDataProvider([
            "query"=>$r,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render("consultas/consulta_35",[
            "dataProvider"=>$d,
            'consulta' => 'Consulta 35',
            'texto' => 'Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine Z.
             Listar todos los campos de la tabla empleados.',
        ]);

    }


}
