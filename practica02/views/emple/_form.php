<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Emple */
/* @var $form yii\widgets\ActiveForm */

$departs=\app\models\Depart::find()->all();
$listDepart=ArrayHelper::map($departs,'dept_no','dnombre');
?>

<div class="emple-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'emp_no')->textInput() ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oficio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dir')->textInput() ?>

    <?= $form->field($model,'fecha_alt')->widget(DatePicker::className(),[
        'options' => [
            'class'=>'form-control',
            'placeholder'=>'Fecha de Alta'
        ],
        'model' => $model,
        'language' => 'es',
        'clientOptions' => [
            'showAnim' => 'fold',
            'showButtonPanel' => 'true',
            'dateFormat' => 'dd/mm/yy',
        ]
    ]);?>

    <?= $form->field($model, 'salario')->textInput() ?>

    <?= $form->field($model, 'comision')->textInput() ?>

    <?= $form->field($model, 'dept_no')->dropDownList(
        $listDepart,
            ['prompt'=>'Selecciona uno ...']
            );
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
