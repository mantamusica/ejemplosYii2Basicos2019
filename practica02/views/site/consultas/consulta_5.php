<?php
/**
 * Created by IntelliJ IDEA.
 * User: Packman
 * Date: 20/03/2019
 * Time: 9:40
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="emple-index">

    <div class="jumbotron">
        <h3><?= $consulta?></h3>
        <p class="lead"><?= $texto?></p>
    </div>
    <div class="row center-block"><?= Html::a('Atrás', ['/site/parte01'], ['class'=>'btn btn-danger']) ?></div>
    <hr>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'columns' => [
            'dept_no',
            'dnombre',
            'loc',
        ],
    ]); ?>
</div>

