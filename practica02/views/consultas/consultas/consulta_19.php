<?php
/**
 * Created by IntelliJ IDEA.
 * User: Packman
 * Date: 20/03/2019
 * Time: 9:40
 */

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="emple-index">
    <div class="jumbotron">
        <h3><?= $consulta?></h3>
        <p class="lead"><?= $texto?></p>
    </div>
    <div class="row center-block"><?= Html::a('Atrás', ['/consultas/index'], ['class'=>'btn btn-danger']) ?></div>
    <hr>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
        'itemView' => '_consulta_19',
    ]);
    ?>
</div>
