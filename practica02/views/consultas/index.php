<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Consultas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'texto',
            'tabla',
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Accioness'
            ],
            ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Consultas',
                    'template' => '{link}',
                    'buttons' => [
                        'link' => function ($url,$model) {
                            return Html::a(Html::a('Consulta', ['/consultas/consulta_' . $model->id], ['class'=>'btn btn-danger']) ,
                                $url);
                        },
            ],
            ],
        ],
    ]); ?>
</div>
