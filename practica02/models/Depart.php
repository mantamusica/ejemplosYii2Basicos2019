<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "depart".
 *
 * @property int $dept_no
 * @property string $dnombre
 * @property string $loc
 *
 * @property Emple[] $emples
 */
class Depart extends \yii\db\ActiveRecord
{
    public $salida;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'depart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dnombre', 'loc'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dept_no' => 'Dept No',
            'dnombre' => 'Dnombre',
            'loc' => 'Loc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmples()
    {
        return $this->hasMany(Emple::className(), ['dept_no' => 'dept_no']);
    }
}
