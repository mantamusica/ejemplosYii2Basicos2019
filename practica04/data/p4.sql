﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 25/03/2019 9:50:38
-- Server version: 5.5.5-10.1.9-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS yii2_practica04;

CREATE DATABASE IF NOT EXISTS yii2_practica04
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE yii2_practica04;

--
-- Create table `delegacion`
--
CREATE TABLE IF NOT EXISTS delegacion (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  poblacion varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create table `trabajadores`
--
CREATE TABLE IF NOT EXISTS trabajadores (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  apellidos varchar(255) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  foto varchar(255) DEFAULT NULL,
  delegacion int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

--
-- Create foreign key
--
ALTER TABLE trabajadores
ADD CONSTRAINT FK_trabajadores_delegacion_id FOREIGN KEY (delegacion)
REFERENCES delegacion (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table delegacion
--
INSERT INTO delegacion VALUES
(1, 'alpe', 'santander', 'pasaje de peña 1 '),
(2, 'saz', 'torrelavega', 'saiz 1'),
(3, 'apd', 'torrelavega', 'vargas 3'),
(4, 'torrelavega', 'laredo', 'laiz 1'),
(5, 'principal', 'asturias', '3'),
(6, 'filial1', 'maliaño', '2');

-- 
-- Dumping data for table trabajadores
--
INSERT INTO trabajadores VALUES
(1, 'ramon', 'abramo ', '1972-11-10', 'ramon.jpg', 1),
(2, 'jose', 'gomez', '2000-10-10', 'josegomez.jpg', 1),
(3, 'silva', 'sanz', '1980-09-20', 'silva.jpg', 2),
(4, 'ana', 'ruiz', '1980-09-20', 'ana.jpg', 3),
(5, 'silvia', 'perez', '1990-09-20', 'silvia.jpg', 3),
(6, 'ana', 'zamora', '1982-10-20', 'ana.jpg', 4),
(7, 'chelo', 'ruiz', '1981-09-01', 'chelo.jpg', 5),
(8, 'pepe', 'plaza', '1982-09-20', 'pepe.jpg', 1),
(9, 'luis', 'pons', '1983-04-21', 'luis.jpg', 1);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;