<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Resultado */
/* @var $form yii\widgets\ActiveForm */

$deportista=\app\models\Deportista::find()->all();
$listDeportistas=ArrayHelper::map($deportista,'id','nombre','id');
$reunion=\app\models\Reunion::find()->all();
$listReuniones=ArrayHelper::map($reunion,'id','nombre','id');

?>

<div class="resultado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'marca')->textInput() ?>

    <?= $form->field($model, 'posicion')->textInput() ?>

    <?= $form->field($model, 'deportista')->textInput() ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listDeportistas,
        ['prompt'=>'Seleciona uno.']
    ) ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listReuniones,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
