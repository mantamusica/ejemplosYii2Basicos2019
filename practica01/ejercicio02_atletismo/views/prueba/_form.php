<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Reunion;
use app\models\Tipo;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Prueba */
/* @var $form yii\widgets\ActiveForm */

$reunion=Reunion::find()->all();
$listReuniones=ArrayHelper::map($reunion,'id','nombre','id');
$tipo=Tipo::find()->all();
$listTipos=ArrayHelper::map($tipo,'id','destino','id');


?>

<div class="prueba-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lugar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora')->textInput() ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listReuniones,
        ['prompt'=>'Seleciona una.']
    ) ?>


    <?= $form->field($model, 'id')->dropDownList(
        $listTipos,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
