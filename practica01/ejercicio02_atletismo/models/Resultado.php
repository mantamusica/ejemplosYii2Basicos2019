<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resultado".
 *
 * @property int $id
 * @property string $marca
 * @property int $posicion
 * @property int $deportista
 * @property int $reunion
 * @property int $prueba
 *
 * @property Deportista $deportista0
 * @property Prueba $prueba0
 * @property Reunion $reunion0
 */
class Resultado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resultado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['marca'], 'safe'],
            [['posicion', 'deportista', 'reunion', 'prueba'], 'integer'],
            [['deportista'], 'exist', 'skipOnError' => true, 'targetClass' => Deportista::className(), 'targetAttribute' => ['deportista' => 'id']],
            [['prueba'], 'exist', 'skipOnError' => true, 'targetClass' => Prueba::className(), 'targetAttribute' => ['prueba' => 'id']],
            [['reunion'], 'exist', 'skipOnError' => true, 'targetClass' => Reunion::className(), 'targetAttribute' => ['reunion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'posicion' => 'Posicion',
            'deportista' => 'Deportista',
            'reunion' => 'Reunion',
            'prueba' => 'Prueba',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeportista0()
    {
        return $this->hasOne(Deportista::className(), ['id' => 'deportista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrueba0()
    {
        return $this->hasOne(Prueba::className(), ['id' => 'prueba']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReunion0()
    {
        return $this->hasOne(Reunion::className(), ['id' => 'reunion']);
    }
}
