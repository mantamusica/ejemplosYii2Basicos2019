<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deportista".
 *
 * @property int $id
 * @property string $nombre
 * @property string $provincia
 * @property int $codigoPostal
 * @property string $dni
 * @property string $domicilio
 * @property string $telefono
 *
 * @property Resultado[] $resultados
 */
class Deportista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deportista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoPostal'], 'integer'],
            [['nombre', 'provincia', 'domicilio'], 'string', 'max' => 255],
            [['dni', 'telefono'], 'string', 'max' => 12],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'provincia' => 'Provincia',
            'codigoPostal' => 'Codigo Postal',
            'dni' => 'Dni',
            'domicilio' => 'Domicilio',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['deportista' => 'id']);
    }
}
