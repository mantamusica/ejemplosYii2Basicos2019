<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reunion".
 *
 * @property int $id
 * @property string $fecha
 * @property string $lugar
 * @property string $nombre
 *
 * @property Prueba[] $pruebas
 * @property Resultado[] $resultados
 */
class Reunion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reunion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['lugar', 'nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'lugar' => 'Lugar',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['reunion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['reunion' => 'id']);
    }
}
