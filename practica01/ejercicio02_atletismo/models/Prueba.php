<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prueba".
 *
 * @property int $id
 * @property string $lugar
 * @property string $hora
 * @property int $reunion
 * @property int $tipo
 *
 * @property Reunion $reunion0
 * @property Tipo $tipo0
 * @property Resultado[] $resultados
 */
class Prueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hora'], 'safe'],
            [['reunion', 'tipo'], 'integer'],
            [['lugar'], 'string', 'max' => 255],
            [['reunion'], 'exist', 'skipOnError' => true, 'targetClass' => Reunion::className(), 'targetAttribute' => ['reunion' => 'id']],
            [['tipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lugar' => 'Lugar',
            'hora' => 'Hora',
            'reunion' => 'Reunion',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReunion0()
    {
        return $this->hasOne(Reunion::className(), ['id' => 'reunion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo0()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['prueba' => 'id']);
    }
}
