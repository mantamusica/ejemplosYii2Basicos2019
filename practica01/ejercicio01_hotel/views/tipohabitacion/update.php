<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tipohabitacion */

$this->title = 'Update Tipohabitacion: ' . $model->idtipo;
$this->params['breadcrumbs'][] = ['label' => 'Tipohabitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtipo, 'url' => ['view', 'id' => $model->idtipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipohabitacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
