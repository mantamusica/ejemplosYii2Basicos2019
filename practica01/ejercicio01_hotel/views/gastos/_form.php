<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Reserva;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */
/* @var $form yii\widgets\ActiveForm */

$reserva=Reserva::find()->all();
$listReservas=ArrayHelper::map($reserva,'codigoReserva','codigoReserva');

?>

<div class="gastos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigoReserva')->dropDownList(
        $listReservas,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <?= $form->field($model, 'descuento')->textInput() ?>

    <?= $form->field($model, 'importe')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
