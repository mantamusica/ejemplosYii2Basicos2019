<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Clientes;
use app\models\Habitacion;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */
/* @var $form yii\widgets\ActiveForm */

$clientes=Clientes::find()->all();
$listClientes=ArrayHelper::map($clientes,'dni','nombre','dni');
$habitacion=Habitacion::find()->all();
$listHabitaciones=ArrayHelper::map($habitacion,'numHabitacion','numHabitacion','idTipo');

?>


<div class="reserva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fechaEntrada')->textInput() ?>

    <?= $form->field($model, 'fechaSalida')->textInput() ?>

    <?= $form->field($model, 'numHabitacion')->dropDownList(
        $listHabitaciones,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <?= $form->field($model, 'dni')->dropDownList(
        $listClientes,
        ['prompt'=>'Selecciona uno.']
    ) ?>

    <?= $form->field($model, 'iva')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
