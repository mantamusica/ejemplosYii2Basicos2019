<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Tipohabitacion;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Habitacion */
/* @var $form yii\widgets\ActiveForm */

$tipo=Tipohabitacion::find()->all();
$listTipos=ArrayHelper::map($tipo,'idtipo','categoria','idtipo');

?>

<div class="habitacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numHabitacion')->textInput() ?>

    <?= $form->field($model, 'idTipo')->dropDownList(
        $listTipos,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
