<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastos".
 *
 * @property int $idGasto
 * @property int $codigoReserva
 * @property double $descuento
 * @property double $importe
 *
 * @property Reserva $codigoReserva0
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoReserva'], 'integer'],
            [['descuento', 'importe'], 'number'],
            [['codigoReserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reserva::className(), 'targetAttribute' => ['codigoReserva' => 'codigoReserva']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idGasto' => 'Id Gasto',
            'codigoReserva' => 'Codigo Reserva',
            'descuento' => 'Descuento',
            'importe' => 'Importe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoReserva0()
    {
        return $this->hasOne(Reserva::className(), ['codigoReserva' => 'codigoReserva']);
    }
}
