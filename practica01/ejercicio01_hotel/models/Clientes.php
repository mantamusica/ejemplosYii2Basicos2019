<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $dni
 * @property string $nombre
 * @property string $apellidos
 * @property string $telefono
 * @property string $email
 * @property string $direccion
 * @property string $fechaNacimiento
 * @property string $poblacion
 * @property int $codPostal
 * @property string $provincia
 * @property string $pais
 *
 * @property Reserva[] $reservas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['codPostal'], 'integer'],
            [['dni'], 'string', 'max' => 10],
            [['nombre', 'apellidos', 'direccion', 'poblacion', 'provincia', 'pais'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 12],
            [['email'], 'string', 'max' => 50],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'poblacion' => 'Poblacion',
            'codPostal' => 'Cod Postal',
            'provincia' => 'Provincia',
            'pais' => 'Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['dni' => 'dni']);
    }
}
