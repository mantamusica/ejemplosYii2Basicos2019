<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $codigoReserva
 * @property string $fechaEntrada
 * @property string $fechaSalida
 * @property int $numHabitacion
 * @property string $dni
 * @property double $iva
 *
 * @property Gastos[] $gastos
 * @property Clientes $dni0
 * @property Habitacion $numHabitacion0
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaEntrada', 'fechaSalida'], 'safe'],
            [['numHabitacion'], 'integer'],
            [['iva'], 'number'],
            [['dni'], 'string', 'max' => 10],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['numHabitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Habitacion::className(), 'targetAttribute' => ['numHabitacion' => 'numHabitacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoReserva' => 'Codigo Reserva',
            'fechaEntrada' => 'Fecha Entrada',
            'fechaSalida' => 'Fecha Salida',
            'numHabitacion' => 'Num Habitacion',
            'dni' => 'Dni',
            'iva' => 'Iva',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gastos::className(), ['codigoReserva' => 'codigoReserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Clientes::className(), ['dni' => 'dni']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumHabitacion0()
    {
        return $this->hasOne(Habitacion::className(), ['numHabitacion' => 'numHabitacion']);
    }
}
