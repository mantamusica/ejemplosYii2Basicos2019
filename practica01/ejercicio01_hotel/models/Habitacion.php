<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habitacion".
 *
 * @property int $numHabitacion
 * @property int $idTipo
 *
 * @property Tipohabitacion $tipo
 * @property Reserva[] $reservas
 */
class Habitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habitacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTipo'], 'integer'],
            [['idTipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tipohabitacion::className(), 'targetAttribute' => ['idTipo' => 'idtipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numHabitacion' => 'Num Habitacion',
            'idTipo' => 'Id Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipohabitacion::className(), ['idtipo' => 'idTipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['numHabitacion' => 'numHabitacion']);
    }
}
