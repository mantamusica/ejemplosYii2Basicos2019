<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipohabitacion".
 *
 * @property int $idtipo
 * @property string $categoria
 * @property string $descripcion
 * @property double $PrecioHabitacion
 *
 * @property Habitacion[] $habitacions
 */
class Tipohabitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipohabitacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PrecioHabitacion'], 'number'],
            [['categoria', 'descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipo' => 'Idtipo',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
            'PrecioHabitacion' => 'Precio Habitacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabitacions()
    {
        return $this->hasMany(Habitacion::className(), ['idTipo' => 'idtipo']);
    }
}
