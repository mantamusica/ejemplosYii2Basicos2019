﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 15/03/2019 9:27:10
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS practica01_emisoras;

CREATE DATABASE IF NOT EXISTS practica01_emisoras
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Set default database
--
USE practica01_emisoras;

--
-- Create table `autor`
--
CREATE TABLE IF NOT EXISTS autor (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  pais varchar(255) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `composicion`
--
CREATE TABLE IF NOT EXISTS composicion (
  id int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(255) DEFAULT NULL,
  estilo varchar(255) DEFAULT NULL,
  autor int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE composicion
ADD CONSTRAINT FK_composicion_autor FOREIGN KEY (autor)
REFERENCES autor (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `interprete`
--
CREATE TABLE IF NOT EXISTS interprete (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  pais varchar(255) DEFAULT NULL,
  fechaNacimiento date DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `version`
--
CREATE TABLE IF NOT EXISTS version (
  id int(11) NOT NULL AUTO_INCREMENT,
  duracion time DEFAULT NULL,
  interprete int(11) DEFAULT NULL,
  composicion int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE version
ADD CONSTRAINT FK_version_composicion FOREIGN KEY (composicion)
REFERENCES composicion (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE version
ADD CONSTRAINT FK_version_interprete FOREIGN KEY (interprete)
REFERENCES interprete (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `emision`
--
CREATE TABLE IF NOT EXISTS emision (
  fechaHora datetime NOT NULL,
  version int(11) DEFAULT NULL,
  composicion int(11) DEFAULT NULL,
  PRIMARY KEY (fechaHora)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE emision
ADD CONSTRAINT FK_emision_composicion FOREIGN KEY (composicion)
REFERENCES composicion (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE emision
ADD CONSTRAINT FK_emision_version FOREIGN KEY (version)
REFERENCES version (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table autor
--
INSERT INTO autor VALUES
(1, 'Bertrada Fährmann', 'Argentina', '1920-03-08'),
(2, 'Heiderose Klotz', 'Kuwait', '1973-07-24'),
(3, 'Joko Adler', 'Bangladesh', '1950-02-07'),
(4, 'Hemma Horkheimer', 'Netherlands', '1983-08-27'),
(5, 'Helwig osenbaum', 'Zimbabwe', '1921-02-17'),
(6, 'Orthey Vogt', 'Suriname', '1924-01-03'),
(7, 'Hannfried Blume', 'Peru', '1972-06-30'),
(8, 'Xenja Kirchner', 'Belgium', '1934-05-29'),
(9, 'Selinda Keller', 'Indonesia', '1919-03-05'),
(10, 'Pontianus Heinz', 'Lebanon', '1967-05-04');

-- 
-- Dumping data for table interprete
--
INSERT INTO interprete VALUES
(1, 'Elwood Ligon', 'New Zealand', '1989-10-17'),
(2, 'Courtney Abernathy', 'Nepal', '1971-04-23'),
(3, 'Rose Aiello', 'India', '1985-07-18'),
(4, 'Natashia Blaylock', 'Bulgaria', '1991-12-06'),
(5, 'Lonna Dion', 'France', '1963-08-28'),
(6, 'Kareem Yazzie', 'Ecuador', '1957-11-28'),
(7, 'Herschel Samples', 'Namibia', '1951-07-21'),
(8, 'Hayden Prichard', 'Fiji', '1952-07-03'),
(9, 'Tosha Dozier', 'Fiji', '1970-01-16'),
(10, 'Antione Baughman', 'Switzerland', '1980-07-02');

-- 
-- Dumping data for table composicion
--
INSERT INTO composicion VALUES
(1, 'Dolor iusto.', 'Eaque commodi est.', 4),
(2, 'Perspiciatis.', 'Dolorem magnam.', 10),
(3, 'Et nemo.', 'Hic quisquam;', 1),
(4, 'Veniam qui.', 'Harum vel eum.', 8),
(5, 'Dignissimos.', 'Esse explicabo;', 4),
(6, 'Consequatur.', 'Voluptatem aliquam.', 4),
(7, 'Fugit ad.', 'Laudantium error.', 3),
(8, 'Cumque.', 'Perferendis quidem.', 9),
(9, 'Qui quibusdam.', 'Ut architecto.', 2),
(10, 'Qui vel animi.', 'Voluptatem culpa.', 10);

-- 
-- Dumping data for table version
--
INSERT INTO version VALUES
(1, '00:34:12', 3, 10),
(2, '00:01:22', 7, 10),
(3, '00:02:18', 1, 1),
(4, '01:36:45', 9, 8),
(5, '01:02:14', 8, 5),
(6, '00:54:37', 3, 5),
(7, '00:02:29', 4, 7),
(8, '00:00:04', 5, 5),
(9, '00:09:01', 2, 9),
(10, '01:52:15', 1, 4);

-- 
-- Dumping data for table emision
--
INSERT INTO emision VALUES
('2003-01-24 17:01:57', 8, 10),
('2008-01-12 14:53:09', 8, 1),
('2010-01-19 12:29:10', 1, 6),
('2011-09-13 14:20:56', 7, 7),
('2013-09-20 11:56:56', 7, 2),
('2013-09-20 11:56:57', 2, 7),
('2015-12-27 22:56:53', 5, 2),
('2015-12-27 22:56:54', 3, 8),
('2018-01-03 20:32:51', 1, 9),
('2018-01-03 20:32:52', 1, 6);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;