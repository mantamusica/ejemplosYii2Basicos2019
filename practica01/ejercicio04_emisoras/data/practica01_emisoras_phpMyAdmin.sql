-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2019 a las 09:27:53
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica01_emisoras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor`
--

CREATE TABLE `autor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `autor`
--

INSERT INTO `autor` (`id`, `nombre`, `pais`, `fechaNacimiento`) VALUES
(1, 'Bertrada Fährmann', 'Argentina', '1920-03-08'),
(2, 'Heiderose Klotz', 'Kuwait', '1973-07-24'),
(3, 'Joko Adler', 'Bangladesh', '1950-02-07'),
(4, 'Hemma Horkheimer', 'Netherlands', '1983-08-27'),
(5, 'Helwig osenbaum', 'Zimbabwe', '1921-02-17'),
(6, 'Orthey Vogt', 'Suriname', '1924-01-03'),
(7, 'Hannfried Blume', 'Peru', '1972-06-30'),
(8, 'Xenja Kirchner', 'Belgium', '1934-05-29'),
(9, 'Selinda Keller', 'Indonesia', '1919-03-05'),
(10, 'Pontianus Heinz', 'Lebanon', '1967-05-04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `composicion`
--

CREATE TABLE `composicion` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `estilo` varchar(255) DEFAULT NULL,
  `autor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `composicion`
--

INSERT INTO `composicion` (`id`, `titulo`, `estilo`, `autor`) VALUES
(1, 'Dolor iusto.', 'Eaque commodi est.', 4),
(2, 'Perspiciatis.', 'Dolorem magnam.', 10),
(3, 'Et nemo.', 'Hic quisquam;', 1),
(4, 'Veniam qui.', 'Harum vel eum.', 8),
(5, 'Dignissimos.', 'Esse explicabo;', 4),
(6, 'Consequatur.', 'Voluptatem aliquam.', 4),
(7, 'Fugit ad.', 'Laudantium error.', 3),
(8, 'Cumque.', 'Perferendis quidem.', 9),
(9, 'Qui quibusdam.', 'Ut architecto.', 2),
(10, 'Qui vel animi.', 'Voluptatem culpa.', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emision`
--

CREATE TABLE `emision` (
  `fechaHora` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `composicion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `emision`
--

INSERT INTO `emision` (`fechaHora`, `version`, `composicion`) VALUES
('2003-01-24 17:01:57', 8, 10),
('2008-01-12 14:53:09', 8, 1),
('2010-01-19 12:29:10', 1, 6),
('2011-09-13 14:20:56', 7, 7),
('2013-09-20 11:56:56', 7, 2),
('2013-09-20 11:56:57', 2, 7),
('2015-12-27 22:56:53', 5, 2),
('2015-12-27 22:56:54', 3, 8),
('2018-01-03 20:32:51', 1, 9),
('2018-01-03 20:32:52', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interprete`
--

CREATE TABLE `interprete` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `interprete`
--

INSERT INTO `interprete` (`id`, `nombre`, `pais`, `fechaNacimiento`) VALUES
(1, 'Elwood Ligon', 'New Zealand', '1989-10-17'),
(2, 'Courtney Abernathy', 'Nepal', '1971-04-23'),
(3, 'Rose Aiello', 'India', '1985-07-18'),
(4, 'Natashia Blaylock', 'Bulgaria', '1991-12-06'),
(5, 'Lonna Dion', 'France', '1963-08-28'),
(6, 'Kareem Yazzie', 'Ecuador', '1957-11-28'),
(7, 'Herschel Samples', 'Namibia', '1951-07-21'),
(8, 'Hayden Prichard', 'Fiji', '1952-07-03'),
(9, 'Tosha Dozier', 'Fiji', '1970-01-16'),
(10, 'Antione Baughman', 'Switzerland', '1980-07-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `version`
--

CREATE TABLE `version` (
  `id` int(11) NOT NULL,
  `duracion` time DEFAULT NULL,
  `interprete` int(11) DEFAULT NULL,
  `composicion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `version`
--

INSERT INTO `version` (`id`, `duracion`, `interprete`, `composicion`) VALUES
(1, '00:34:12', 3, 10),
(2, '00:01:22', 7, 10),
(3, '00:02:18', 1, 1),
(4, '01:36:45', 9, 8),
(5, '01:02:14', 8, 5),
(6, '00:54:37', 3, 5),
(7, '00:02:29', 4, 7),
(8, '00:00:04', 5, 5),
(9, '00:09:01', 2, 9),
(10, '01:52:15', 1, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `composicion`
--
ALTER TABLE `composicion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_composicion_autor` (`autor`);

--
-- Indices de la tabla `emision`
--
ALTER TABLE `emision`
  ADD PRIMARY KEY (`fechaHora`),
  ADD KEY `FK_emision_composicion` (`composicion`),
  ADD KEY `FK_emision_version` (`version`);

--
-- Indices de la tabla `interprete`
--
ALTER TABLE `interprete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_version_interprete` (`interprete`),
  ADD KEY `FK_version_composicion` (`composicion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autor`
--
ALTER TABLE `autor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `composicion`
--
ALTER TABLE `composicion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `interprete`
--
ALTER TABLE `interprete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `version`
--
ALTER TABLE `version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `composicion`
--
ALTER TABLE `composicion`
  ADD CONSTRAINT `FK_composicion_autor` FOREIGN KEY (`autor`) REFERENCES `autor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `emision`
--
ALTER TABLE `emision`
  ADD CONSTRAINT `FK_emision_composicion` FOREIGN KEY (`composicion`) REFERENCES `composicion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_emision_version` FOREIGN KEY (`version`) REFERENCES `version` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `version`
--
ALTER TABLE `version`
  ADD CONSTRAINT `FK_version_composicion` FOREIGN KEY (`composicion`) REFERENCES `composicion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_version_interprete` FOREIGN KEY (`interprete`) REFERENCES `interprete` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
