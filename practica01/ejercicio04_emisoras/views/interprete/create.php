<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Interprete */

$this->title = 'Create Interprete';
$this->params['breadcrumbs'][] = ['label' => 'Interpretes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interprete-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
