<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emision */

$this->title = 'Update Emision: ' . $model->fechaHora;
$this->params['breadcrumbs'][] = ['label' => 'Emisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fechaHora, 'url' => ['view', 'id' => $model->fechaHora]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emision-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
