<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emision */

$this->title = 'Create Emision';
$this->params['breadcrumbs'][] = ['label' => 'Emisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emision-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
