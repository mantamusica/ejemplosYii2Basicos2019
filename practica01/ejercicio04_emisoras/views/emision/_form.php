<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Composicion;
use app\models\Version;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Emision */
/* @var $form yii\widgets\ActiveForm */

$comps=Composicion::find()->all();
$listComps=ArrayHelper::map($comps,'id','titulo','id');
$vers=Version::find()->all();
$listVers=ArrayHelper::map($vers,'id','duracion','id');

?>

<div class="emision-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fechaHora')->textInput() ?>

<!--    --><?/*= $form->field($model, 'id')->dropDownList(
        $listVers,
        ['prompt'=>'Seleciona una.']
    ) */?>

    <?= $form->field($model, 'composicion')->textInput() ?>

<!--    --><?/*= $form->field($model, 'id')->dropDownList(
        $listComps,
        ['prompt'=>'Seleciona una.']
    ) */?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
