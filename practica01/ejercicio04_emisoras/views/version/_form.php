<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Interprete;
use app\models\Composicion;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Version */
/* @var $form yii\widgets\ActiveForm */

$inters=Interprete::find()->all();
$listInters=ArrayHelper::map($inters,'id','nombre','id');
$comps=Composicion::find()->all();
$listComps=ArrayHelper::map($comps,'id','titulo','id');

?>

<div class="version-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'duracion')->textInput() ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listInters,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listComps,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
