<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Composicion */

$this->title = 'Create Composicion';
$this->params['breadcrumbs'][] = ['label' => 'Composicions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="composicion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
