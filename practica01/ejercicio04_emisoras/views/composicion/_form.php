<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Autor;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Composicion */
/* @var $form yii\widgets\ActiveForm */

$autores=Autor::find()->all();
$listAutores=ArrayHelper::map($autores,'id','nombre','id');

?>

<div class="composicion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estilo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listAutores,
        ['prompt'=>'Seleciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
