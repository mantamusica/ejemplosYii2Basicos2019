<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "version".
 *
 * @property int $id
 * @property string $duracion
 * @property int $interprete
 * @property int $composicion
 *
 * @property Emision[] $emisions
 * @property Composicion $composicion0
 * @property Interprete $interprete0
 */
class Version extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'version';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion'], 'safe'],
            [['interprete', 'composicion'], 'integer'],
            [['composicion'], 'exist', 'skipOnError' => true, 'targetClass' => Composicion::className(), 'targetAttribute' => ['composicion' => 'id']],
            [['interprete'], 'exist', 'skipOnError' => true, 'targetClass' => Interprete::className(), 'targetAttribute' => ['interprete' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'duracion' => 'Duracion',
            'interprete' => 'Interprete',
            'composicion' => 'Composicion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmisions()
    {
        return $this->hasMany(Emision::className(), ['version' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComposicion0()
    {
        return $this->hasOne(Composicion::className(), ['id' => 'composicion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterprete0()
    {
        return $this->hasOne(Interprete::className(), ['id' => 'interprete']);
    }
}
