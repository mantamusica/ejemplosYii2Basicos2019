<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "composicion".
 *
 * @property int $id
 * @property string $titulo
 * @property string $estilo
 * @property int $autor
 *
 * @property Autor $autor0
 * @property Emision[] $emisions
 * @property Version[] $versions
 */
class Composicion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'composicion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor'], 'integer'],
            [['titulo', 'estilo'], 'string', 'max' => 255],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::className(), 'targetAttribute' => ['autor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'estilo' => 'Estilo',
            'autor' => 'Autor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autor::className(), ['id' => 'autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmisions()
    {
        return $this->hasMany(Emision::className(), ['composicion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersions()
    {
        return $this->hasMany(Version::className(), ['composicion' => 'id']);
    }
}
