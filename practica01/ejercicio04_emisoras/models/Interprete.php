<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interprete".
 *
 * @property int $id
 * @property string $nombre
 * @property string $pais
 * @property string $fechaNacimiento
 *
 * @property Version[] $versions
 */
class Interprete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'interprete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'safe'],
            [['nombre', 'pais'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'pais' => 'Pais',
            'fechaNacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersions()
    {
        return $this->hasMany(Version::className(), ['interprete' => 'id']);
    }
}
