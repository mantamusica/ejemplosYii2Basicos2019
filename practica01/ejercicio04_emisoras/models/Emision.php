<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emision".
 *
 * @property string $fechaHora
 * @property int $version
 * @property int $composicion
 *
 * @property Composicion $composicion0
 * @property Version $version0
 */
class Emision extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emision';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaHora'], 'required'],
            [['fechaHora'], 'safe'],
            [['version', 'composicion'], 'integer'],
            [['fechaHora'], 'unique'],
            [['composicion'], 'exist', 'skipOnError' => true, 'targetClass' => Composicion::className(), 'targetAttribute' => ['composicion' => 'id']],
            [['version'], 'exist', 'skipOnError' => true, 'targetClass' => Version::className(), 'targetAttribute' => ['version' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fechaHora' => 'Fecha Hora',
            'version' => 'Version',
            'composicion' => 'Composicion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComposicion0()
    {
        return $this->hasOne(Composicion::className(), ['id' => 'composicion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersion0()
    {
        return $this->hasOne(Version::className(), ['id' => 'version']);
    }
}
