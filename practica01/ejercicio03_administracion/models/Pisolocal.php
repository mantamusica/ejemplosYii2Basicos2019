<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pisolocal".
 *
 * @property int $id
 * @property int $comunidad
 * @property int $propietario
 * @property int $planta
 * @property string $letra
 * @property int $coeficiente
 * @property string $descripcion
 *
 * @property Gastosingresos[] $gastosingresos
 * @property Comunidad $comunidad0
 * @property Propietario $propietario0
 */
class Pisolocal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pisolocal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comunidad', 'propietario', 'planta', 'coeficiente'], 'integer'],
            [['letra'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 255],
            [['comunidad'], 'exist', 'skipOnError' => true, 'targetClass' => Comunidad::className(), 'targetAttribute' => ['comunidad' => 'id']],
            [['propietario'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['propietario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comunidad' => 'Comunidad',
            'propietario' => 'Propietario',
            'planta' => 'Planta',
            'letra' => 'Letra',
            'coeficiente' => 'Coeficiente',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastosingresos()
    {
        return $this->hasMany(Gastosingresos::className(), ['pisoLocal' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunidad0()
    {
        return $this->hasOne(Comunidad::className(), ['id' => 'comunidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario0()
    {
        return $this->hasOne(Propietario::className(), ['id' => 'propietario']);
    }
}
