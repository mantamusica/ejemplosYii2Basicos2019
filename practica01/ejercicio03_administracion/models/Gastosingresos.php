<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastosingresos".
 *
 * @property int $id
 * @property string $fecha
 * @property string $concepto
 * @property double $importe
 * @property int $comunidad
 * @property int $pisoLocal
 *
 * @property Comunidad $comunidad0
 * @property Pisolocal $pisoLocal0
 */
class Gastosingresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastosingresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['importe'], 'number'],
            [['comunidad', 'pisoLocal'], 'integer'],
            [['concepto'], 'string', 'max' => 255],
            [['comunidad'], 'exist', 'skipOnError' => true, 'targetClass' => Comunidad::className(), 'targetAttribute' => ['comunidad' => 'id']],
            [['pisoLocal'], 'exist', 'skipOnError' => true, 'targetClass' => Pisolocal::className(), 'targetAttribute' => ['pisoLocal' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'importe' => 'Importe',
            'comunidad' => 'Comunidad',
            'pisoLocal' => 'Piso Local',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComunidad0()
    {
        return $this->hasOne(Comunidad::className(), ['id' => 'comunidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPisoLocal0()
    {
        return $this->hasOne(Pisolocal::className(), ['id' => 'pisoLocal']);
    }
}
