<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propietario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $direccion
 * @property string $dni
 * @property int $codigoPostal
 * @property string $telefono
 *
 * @property Pisolocal[] $pisolocals
 */
class Propietario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propietario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoPostal'], 'integer'],
            [['nombre', 'direccion'], 'string', 'max' => 255],
            [['dni', 'telefono'], 'string', 'max' => 20],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'dni' => 'Dni',
            'codigoPostal' => 'Codigo Postal',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPisolocals()
    {
        return $this->hasMany(Pisolocal::className(), ['propietario' => 'id']);
    }
}
