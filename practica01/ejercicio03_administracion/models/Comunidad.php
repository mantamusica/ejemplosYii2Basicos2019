<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comunidad".
 *
 * @property int $id
 * @property string $direccion
 * @property string $codigoPostal
 * @property string $provincia
 *
 * @property Gastosingresos[] $gastosingresos
 * @property Pisolocal[] $pisolocals
 */
class Comunidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comunidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direccion', 'codigoPostal', 'provincia'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'direccion' => 'Direccion',
            'codigoPostal' => 'Codigo Postal',
            'provincia' => 'Provincia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastosingresos()
    {
        return $this->hasMany(Gastosingresos::className(), ['comunidad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPisolocals()
    {
        return $this->hasMany(Pisolocal::className(), ['comunidad' => 'id']);
    }
}
