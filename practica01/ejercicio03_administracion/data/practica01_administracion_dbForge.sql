--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 14/03/2019 22:05:21
-- Server version: 5.5.5-10.1.30-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP DATABASE IF EXISTS practica01_administracion;

CREATE DATABASE IF NOT EXISTS practica01_administracion
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Set default database
--
USE practica01_administracion;

--
-- Create table `propietario`
--
CREATE TABLE IF NOT EXISTS propietario (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  dni char(20) DEFAULT NULL,
  codigoPostal int(5) DEFAULT NULL,
  telefono char(20) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 51,
AVG_ROW_LENGTH = 327,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `dni` on table `propietario`
--
ALTER TABLE propietario
ADD UNIQUE INDEX dni (dni);

--
-- Create table `comunidad`
--
CREATE TABLE IF NOT EXISTS comunidad (
  id int(11) NOT NULL AUTO_INCREMENT,
  direccion varchar(255) DEFAULT NULL,
  codigoPostal varchar(255) DEFAULT NULL,
  provincia varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 51,
AVG_ROW_LENGTH = 327,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `pisolocal`
--
CREATE TABLE IF NOT EXISTS pisolocal (
  id int(11) NOT NULL AUTO_INCREMENT,
  comunidad int(11) DEFAULT NULL,
  propietario int(11) DEFAULT NULL,
  planta int(11) DEFAULT NULL,
  letra char(20) DEFAULT NULL,
  coeficiente int(11) DEFAULT NULL,
  descripcion varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 51,
AVG_ROW_LENGTH = 327,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE pisolocal
ADD CONSTRAINT FK_pisolocal_comunidad FOREIGN KEY (comunidad)
REFERENCES comunidad (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE pisolocal
ADD CONSTRAINT FK_pisolocal_propietario FOREIGN KEY (propietario)
REFERENCES propietario (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create table `gastosingresos`
--
CREATE TABLE IF NOT EXISTS gastosingresos (
  id int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  concepto varchar(255) DEFAULT NULL,
  importe float DEFAULT NULL,
  comunidad int(11) DEFAULT NULL,
  pisoLocal int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 51,
AVG_ROW_LENGTH = 327,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE gastosingresos
ADD CONSTRAINT FK_gastosingresos_comunidad FOREIGN KEY (comunidad)
REFERENCES comunidad (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Create foreign key
--
ALTER TABLE gastosingresos
ADD CONSTRAINT FK_gastosingresos_pisoLocal FOREIGN KEY (pisoLocal)
REFERENCES pisolocal (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table propietario
--
INSERT INTO propietario VALUES
(1, 'Ilsabein Marschner', 'Billing', 'XFIPQ2836P', 52352, '+52 41 2393 1767'),
(2, 'Candie chacht', 'Primary', 'NWPGE4158V', 25919, '+33 3 18 95 10 73'),
(3, 'Berngar Seidlitz', 'Shipping', 'MZTJD1388X', 47684, '+44 299 816 3992'),
(4, 'Kalixtus Ebel', 'Billing', 'DQBHK8684T', 25103, '+44 4835 48 6840'),
(5, 'Hubertine Lengefeld', 'Office', 'OQKBG7837A', 54203, '+49-6577-702719'),
(6, 'Seraphim Gebbels', 'Billing', 'GMQBK1499L', 28304, '+44 047 944 6714'),
(7, 'Gerwig Baade', 'Primary', 'MHMAB8619X', 58678, '+49 (5710) 567804'),
(8, 'Kornelis Engel', 'Shipping', 'UWSKX3932R', 28290, '+380 25 651-332-9'),
(9, 'Walpurga Eichenwald', 'Primary', 'ZRHHO8966S', 36383, '+49 8078 486930'),
(10, 'Nastja agel', 'Primary', 'TATFA3215E', 29047, '+49-0977-614490'),
(11, 'Hademar chirrmann', 'Primary', 'DCGTQ5593I', 52080, '+32 5 361 55 95'),
(12, 'Blandine Burgdorf', 'Office', 'HNOCN1732X', 30243, '+44 2283 50 9733'),
(13, 'Adlin  Anger', 'Delivery', 'MWJFE5991O', 29528, '+971 5 393 9090'),
(14, 'Richmute Wolf', 'Billing', 'ELDLZ3333B', 27927, '+44 179 003 6217'),
(15, 'Adlin  Joseph', 'Primary', 'MHLGH8123P', 46955, '+55 41 7621-3168'),
(16, 'Petia Grundig', 'Delivery', 'DNQFS8433I', 59603, '+49 (5695) 405789'),
(17, 'Claus M�bius', 'Delivery', 'WFXHZ2386D', 26489, '+44 0145 989243'),
(18, 'Gundela Kunze', 'Office', 'ICLBF3324P', 31137, '+971 1 630 1009'),
(19, 'Finnia chlechter', 'Delivery', 'YZGBK2383G', 51431, '+44 423 640 6716'),
(20, 'Clovis Kloz', 'Billing', 'SEGLU1929Z', 49807, '+49 (9720) 629876'),
(21, 'Pinkas Liebknecht', 'Home', 'GPNLI8725T', 21223, '+380 80 755-192-4'),
(22, 'Heidelinde Thalberg', 'Office', 'CZTLW2918E', 38660, '+52 07 7638 5917'),
(23, 'Quintin Ittenbach', 'Office', 'YXCPR8688G', 38008, '+971 9 691 6739'),
(24, 'Alenia  chefer', 'Delivery', 'CIPBN9912C', 32925, '+55 17 1097-6574'),
(25, 'Nikas Kretschmer', 'Primary', 'HUDGG4241I', 56414, '+49 1776 573841'),
(26, 'Sigi Manstein', 'Home', 'QFWPN6838N', 24930, '+1 303-477-7121'),
(27, 'Lennik ander', 'Billing', 'ALZKC4732L', 57165, '+380 69 262-268-6'),
(28, 'Hannfried cheffer', 'Office', 'BONTC7491G', 59893, '+44 9040 934877'),
(29, 'Lutha Gr�nenfelder', 'Delivery', 'CJEGD9159U', 39192, '+380 25 815-478-0'),
(30, 'Elise Abert', 'Office', 'QRHFI9229Y', 33479, '+55 65 2230-4146'),
(31, 'Borchard Abert', 'Primary', 'QTUTN5527R', 52316, '+44 5537 755809'),
(32, 'Alfonsina  Unger', 'Delivery', 'MKFLH6648J', 57492, '+44 404 094 2740'),
(33, 'Georgie Appel', 'Shipping', 'BPJJS1348L', 42991, '+33 4 06 29 95 58'),
(34, 'Polyxena Linger', 'Shipping', 'AQBPW8988N', 58414, '+49-1500-521874'),
(35, 'Thusnelda berl�nder', 'Office', 'HWUCW3523E', 28675, '+49 (3093) 482923'),
(36, 'Inola Assmann', 'Home', 'MZAAE8561K', 48945, '+44 8402 67 9373'),
(37, 'Abbeygail  Block', 'Billing', 'FNVHZ9867E', 24761, '+420 289 370 200'),
(38, 'Landulf Brandt', 'Home', 'DVATP6223K', 48144, '+49 2527 990954'),
(39, 'Halrun Adler', 'Billing', 'XGMGU6884K', 24236, '+49-3893-656181'),
(40, 'Eckard Kirschner', 'Home', 'BFVJT6587P', 37742, '+49 9046 959836'),
(41, 'Menowin Anger', 'Primary', 'GASCE9181R', 33968, '+33 5 24 91 69 44'),
(42, 'Hubertina Brenner', 'Home', 'CUNTZ6412J', 22599, '+44 8703 746799'),
(43, 'Karl-Heinz Kurz', 'Delivery', 'JFQFS2159Q', 45157, '+32 0 763 83 31'),
(44, 'Avera  Ittenbach', 'Shipping', 'ASZLW3732A', 51619, '+971 0 998 6924'),
(45, 'Chlodomer ascher', 'Shipping', 'CZABE4693I', 39949, '+971 0 137 4213'),
(46, 'C�sar Kronberg', 'Home', 'DHATY1956T', 41864, '+44 308 911 6363'),
(47, 'Adina  Ude', 'Delivery', 'YHHLL4929H', 54678, '+52 36 5837 2105'),
(48, 'Annchristin later', 'Shipping', 'WRSAT1328W', 36718, '+33 5 37 15 83 62'),
(49, 'Annamaria ichter', 'Primary', 'AXULL3268B', 26678, '+971 3 937 5673'),
(50, 'Br�ne Strasser', 'Billing', 'WXDJX2424X', 47687, '+33 0 67 66 95 66');

-- 
-- Dumping data for table comunidad
--
INSERT INTO comunidad VALUES
(1, 'Kaufingerstra�e 8, 84074, W�rthsee', '4751', 'Australian Capital Territory'),
(2, 'An der Hauptfeuerwache 4, 97537, Dannewerk', '3137', 'Queensland'),
(3, 'Prielmayerstra�e 1, 46283, Ehrenberg', '2617', 'Victoria'),
(4, 'Helene-Lange-Weg 73a, 61025, Aach', '0801', 'Northern Territory'),
(5, 'Michael-Huber-Weg 2, 63842, Reinholterode', '3138', 'New South Wales'),
(6, 'Adi-Maislinger-Stra�e 14, 11721, Grafrath', '2017', 'Tasmania'),
(7, 'Siegfriedstra�e 1, 11980, Anning', '6306', 'Western Australia'),
(8, 'Schumannstra�e 5, 59665, Sch�tzenhof', '2618', 'South Australia'),
(9, 'Gebsattelstra�e 18, 04459, Grasberg', '3139', 'Northern Territory'),
(10, 'Adalbert-Stifter-Stra�e 6, 11763, Haina', '3816', 'Australian Capital Territory'),
(11, 'Karl-Scharnagl-Ring 4, 24900, Poyenberg', '7030', 'Victoria'),
(12, 'Adalbert-Stifter-Stra�e 5b, 82434, Jevenberg', '0804', 'New South Wales'),
(13, 'Kohlstra�e 2, 52848, Gro� Wittensee', '5422', 'Western Australia'),
(14, 'Maffeistra�e 4, 88602, Pelzerhaus', '2018', 'Queensland'),
(15, 'Adi-Maislinger-Stra�e 16c, 71929, Abbendorf', '4407', 'South Australia'),
(16, 'Herzogspitalstra�e 5d, 02886, Lindlar', '6308', 'Tasmania'),
(17, 'Baldeplatz 2e, 57171, Ahrensfelde', '2619', 'South Australia'),
(18, 'Simmernstra�e 15-17, 25622, Ritzgerode', '3140', 'Victoria'),
(19, 'Barbarastra�e 3, 76628, Platkow', '0810', 'Tasmania'),
(20, 'Jakob-Klar-Stra�e 4, 66376, Pogeez', '2019', 'Northern Territory'),
(21, 'Besselstra�e 14f, 74219, Melz', '0811', 'Australian Capital Territory'),
(22, 'Welserstra�e 8, 49809, Marktgraitz', '7050', 'Queensland'),
(23, 'Am Einla� 1, 29482, G�schitz', '3818', 'New South Wales'),
(24, 'Sedlmayrstra�e 2, 73267, H�rgertshausen', '4408', 'Western Australia'),
(25, 'Isartorplatz 78d, 05094, Altusried', '6309', 'Queensland'),
(26, 'Am Bergsteig 89, 32494, Bochow', '7051', 'Australian Capital Territory'),
(27, 'Mannlichstra�e 2, 37514, Tiefengruft', '4753', 'New South Wales'),
(28, 'Adi-Maislinger-Stra�e 2, 67223, Abbau Ader', '5431', 'Tasmania'),
(29, 'Habsburgerstra�e 4, 22689, Kunreuth', '4754', 'Western Australia'),
(30, 'Nordendstra�e 7, 73862, Frielendorf', '6311', 'Northern Territory'),
(31, 'Grimmstra�e 1, 06943, Wendtorf', '7052', 'Victoria'),
(32, 'Gy�lingstra�e 6, 94908, Schuttertal', '5432', 'South Australia'),
(33, 'Rolandstra�e 1, 57655, Heinrichsthal', '3820', 'Northern Territory'),
(34, 'Newtonstra�e 30, 68544, Abtsdorf', '4410', 'Queensland'),
(35, 'Lily-Braun-Weg 2, 66765, Aalen', '6312', 'New South Wales'),
(36, 'Aschheimer Stra�e 26, 52002, Hohenwarth', '7053', 'Victoria'),
(37, 'Anglerstra�e 13, 87309, Hartenholm', '6313', 'Western Australia'),
(38, 'Adalbert-Stifter-Stra�e 79a, 46973, Hassendorf', '2620', 'South Australia'),
(39, 'Westerm�hlstra�e 9, 45104, Ohmden', '4756', 'Australian Capital Territory'),
(40, 'Hartliebstra�e 2, 29623, R�terberg', '5433', 'Tasmania'),
(41, 'Adi-Maislinger-Stra�e 37, 52352, Spring', '7054', 'South Australia'),
(42, 'Anglerstra�e 5, 27477, Aulosen', '3141', 'Australian Capital Territory'),
(43, 'Leibweg 13-16, 91764, Wartbichl', '2621', 'Western Australia'),
(44, 'Karl-Preis-Platz 2, 62504, Hohenwutzen', '3142', 'Tasmania'),
(45, 'Zumpestra�e 9c, 77195, Marlow', '6315', 'Northern Territory'),
(46, 'Ackermannstra�e 29d, 88297, Lampaden', '2622', 'Victoria'),
(47, 'Tassiloplatz 24, 57721, Aach', '3143', 'New South Wales'),
(48, 'Merzstra�e 1, 65056, Oberachthal', '2020', 'Queensland'),
(49, 'Pienzenauerstra�e 2d, 56449, Bissersheim', '2623', 'Queensland'),
(50, 'Schackstra�e 2, 23168, Rattenberg', '7055', 'Victoria');

-- 
-- Dumping data for table pisolocal
--
INSERT INTO pisolocal VALUES
(1, 22, 46, 9, 't', 14, 'Vel accusantium perspiciatis. Voluptatem explicabo est. Consequatur assumenda expedita; quis iure iste. Quas et quidem; cumque perspiciatis ullam. Omnis aspernatur quidem. Aliquam vero reprehenderit.\r\n'),
(2, 1, 44, 8, 'z', 16, 'Et facere iste. Est et qui. Mollitia dicta voluptas. Dolore qui velit. In non eius. Corporis quis est. Expedita qui corporis...\r\nEt aut perspiciatis. Voluptatem omnis porro! Aut harum est. Quas quae!\r\n'),
(3, 27, 26, 11, 'u', 12, 'Nisi praesentium excepturi iste odit placeat qui hic nobis dolorem.'),
(4, 22, 33, 11, 'f', 13, 'Et voluptatem soluta. Quos vel ducimus. Repellat provident beatae! Eum vitae quis.\r\nOmnis a quia. Aut quo mollitia. Natus fugiat ut? Explicabo sed ut. Est sint fuga; voluptatem quia vel. Beatae ut ex.\r\n'),
(5, 39, 28, 7, 'f', 12, 'Ea est itaque. Officiis est perspiciatis! Recusandae quia quasi. Quas eveniet minus.\r\nEst non et. Ut perspiciatis delectus. Nostrum et doloribus! Assumenda vel et. Facilis soluta expedita! Nihil hic.'),
(6, 30, 5, 2, 'g', 10, 'Ut et qui nisi aut mollitia ut iure enim. Alias voluptates itaque eos voluptatem; ea est aperiam ut ullam et sit enim est quasi.'),
(7, 46, 23, 14, 'i', 18, 'Quia excepturi quidem; et est distinctio; sed distinctio labore. Commodi provident consequuntur. Tempora deserunt asperiores. Voluptatem error architecto.\r\nQui sit qui. Consectetur delectus veritatis.\r\n'),
(8, 38, 35, 19, 'w', 15, 'Ut ratione ut inventore. Et consequatur perspiciatis sequi fugiat voluptatem.\r\nSint iste exercitationem. Eligendi repudiandae vitae. Sint possimus aut.\r\nNon et voluptatem. Consequuntur numquam iste!'),
(9, 17, 46, 6, 'u', 16, 'Nesciunt magni assumenda. Alias quia quae! Ipsa id omnis. Id iste temporibus! Velit sit tempora. Dolore temporibus accusamus. Sit quam eum. Tenetur natus earum.\r\nCum harum sint. Est sunt error. Unde.\r\n'),
(10, 46, 32, 5, 'd', 14, 'Ad doloremque dolorem. Error quas sit.\r\nEt nisi distinctio. Temporibus veritatis sed. Voluptatem qui molestiae. Quis eos voluptates? Et eos soluta. Excepturi asperiores qui! Ad dicta itaque. Delectus.'),
(11, 48, 39, 0, 'u', 15, 'Delectus veritatis similique. Maiores fugit quam? Sit quibusdam numquam! Temporibus rerum eum. Sed dolorem quo? Et nam qui! Et hic excepturi. Quo consectetur omnis. Qui libero sequi. Quas omnis.\r\n'),
(12, 16, 13, 14, 'u', 19, 'Dolores voluptas voluptatem. Nulla et perspiciatis. Nemo ratione molestias. Numquam quae quia! Rem non ducimus. Consequatur adipisci in. Velit qui ut. Vitae deserunt autem.\r\nQuasi sint corrupti.\r\nNatus.'),
(13, 3, 8, 2, 'f', 10, 'Dicta praesentium velit. Nisi unde obcaecati; neque perspiciatis magni. Incidunt natus molestias. Accusantium cumque quasi.\r\nIpsa dolore accusantium. Cupiditate error eum. Quae nihil tenetur. Dolor.\r\n'),
(14, 10, 28, 11, 'm', 20, 'Voluptatem sint quidem error deleniti minima magnam nobis; sit omnis a porro dolor facere.\r\nMagni unde sit, voluptatem dolores ex ut harum natus voluptatem.'),
(15, 43, 7, 8, 'j', 17, 'Itaque numquam nisi autem fugit voluptatem omnis.\r\nSed et suscipit ea sit voluptatem magnam numquam ullam. Placeat praesentium pariatur excepturi minima aut qui nam! Quaerat dicta saepe; nihil est sit.'),
(16, 47, 23, 8, 'u', 10, 'Quos saepe qui. Et consequatur ad! Maiores ab hic. Ut facilis molestiae. Qui molestias perspiciatis. Distinctio at debitis! Dicta pariatur aut. Reprehenderit repellendus amet. Voluptatum accusantium!\r\n'),
(17, 23, 47, 1, 'm', 13, 'Ut maiores reprehenderit; iste pariatur beatae? Debitis sapiente adipisci! Rem unde iste. Consequatur ex et! Minima fugit molestiae. At non rerum. Eos dignissimos error. Exercitationem qui quod.\r\n'),
(18, 8, 26, 20, 'd', 14, 'Nihil voluptatem amet. Aliquam rem vero.\r\nEum odio necessitatibus. Dolores perspiciatis magnam. Perferendis repellendus corporis. Nesciunt rerum molestias. Nam sit labore. Suscipit neque consequuntur;\r\n'),
(19, 32, 32, 5, 'o', 20, 'Tempora autem doloribus. Velit distinctio ad. Culpa est voluptatem. Non autem quidem. Molestias cumque dignissimos.\r\nMagnam corporis et. Mollitia et itaque! Perspiciatis ut in. Maiores sed repellendus;'),
(20, 45, 38, 7, 'g', 11, 'Quisquam et reiciendis ipsum; ab ipsum sit fuga sint repudiandae molestiae quia natus. Qui sit maxime. Unde nemo aut. Harum non aut? Distinctio consequuntur natus.'),
(21, 1, 49, 10, 't', 20, 'Ut error iste. Ut unde quia; aut exercitationem eius.\r\nEos sed error. Omnis quis sit. Aut soluta corporis! Hic sint esse. Qui perferendis illo? Ratione ut nihil! Voluptas accusamus incidunt.\r\nNon cumque.'),
(22, 36, 28, 19, 'e', 18, 'Non omnis voluptatibus, veniam asperiores error sapiente quis voluptate veritatis. Amet eum quaerat magnam et eos vero quaerat mollitia explicabo.'),
(23, 8, 37, 6, 'z', 14, 'Ut omnis facilis; assumenda qui voluptates. Odio natus vel. Unde ut quisquam.\r\nFugiat ut odit. Sequi sunt at. Natus vero incidunt! Fuga ex molestias. Obcaecati totam aut? Voluptatem vel asperiores. Ab.\r\n'),
(24, 12, 13, 20, 'a', 15, 'Dolore sit voluptatem. Nam totam ipsum. Voluptatum eum et. Quae sit distinctio...\r\nPraesentium magni aliquam. Debitis similique pariatur.\r\nEnim consectetur cum. Magnam tenetur excepturi. Beatae sed illum.'),
(25, 24, 32, 9, 'c', 10, 'Iste eligendi optio. Omnis voluptatem ut.\r\nSapiente omnis sed. Unde aut quasi. Omnis facilis aperiam! Et aliquam omnis. Nemo perspiciatis laboriosam! Maiores quo perferendis. Doloremque quam enim.\r\n'),
(26, 27, 25, 15, 'x', 19, 'Quasi molestias maiores. Dolorem sint sit. Voluptas aut quasi! Reprehenderit maxime iusto; rem est omnis; enim sit voluptas; voluptate nulla molestiae. Repellendus voluptatem expedita! Cumque suscipit.'),
(27, 34, 49, 6, 'n', 16, 'Voluptate qui reprehenderit. Maxime quia nihil.\r\nExpedita eius quam. Voluptatem delectus ut. Aliquam debitis sit. Ipsum incidunt sit. Aperiam ab ut. Veniam et sint; velit corporis exercitationem?\r\n'),
(28, 15, 6, 0, 'r', 18, 'Nostrum cupiditate vero. Voluptatem ab illo.\r\nIllo cumque et. Aperiam molestiae dolorem. Sed veniam soluta. Qui culpa dolor. Fugit magnam omnis. Accusantium qui libero! A quisquam dolorem. Dicta.'),
(29, 12, 4, 3, 'a', 13, 'Sint dolores omnis. Vero et quaerat.\r\nQui repellat quae; aut consequatur eaque; porro ipsa consequatur. Eaque omnis aut. Aliquid consequatur repellendus! Ea sequi voluptas. Et qui dolor. Qui rerum.\r\n'),
(30, 21, 6, 13, 'r', 17, 'Nemo sapiente quos. Voluptatem minus nostrum! Dolore minima maiores. Natus labore unde. Ea possimus natus! Laboriosam sed omnis. Et nihil odit. Et sed id. Voluptatem omnis quia.\r\nIpsa voluptatem.\r\n'),
(31, 2, 20, 17, 'u', 13, 'Deserunt reiciendis aut. Dolorum natus et.\r\nDelectus omnis et. Natus veritatis molestias! Quia animi facere? Enim perspiciatis repudiandae! Sapiente enim sint. Odit distinctio architecto.\r\nAccusantium.'),
(32, 44, 1, 5, 't', 16, 'Voluptas adipisci aliquid illo iste est debitis. Aperiam dicta voluptas explicabo iste modi. Eum provident delectus. Et sit error? Voluptas consequatur quod numquam nemo vel quia.'),
(33, 17, 44, 15, 'd', 16, 'Quia exercitationem fugiat. Hic veritatis minima. Error ipsa amet. Mollitia officiis nesciunt? Odio labore cupiditate. Nihil placeat qui; iure enim aut? Reiciendis iusto unde. Cupiditate consequatur?\r\n'),
(34, 9, 12, 9, 'w', 18, 'Sunt praesentium fugiat. Ducimus excepturi rem.\r\nEsse vero et. Similique enim reiciendis. Incidunt labore aliquid! In debitis animi? Reprehenderit sapiente velit.\r\nEt ut facilis. Eum qui nam! Id.'),
(35, 7, 4, 20, 'z', 16, 'Sit et autem quas quam similique atque. Aut inventore ullam voluptas. Laboriosam temporibus ratione. Omnis ut nisi. Voluptatem soluta id? Iste voluptas error. Et fugiat illum.'),
(36, 34, 37, 1, 't', 13, 'Incidunt quidem vero.\r\nConsectetur numquam porro. Repellendus et ut!\r\nSunt quaerat voluptas. Id dolorem ullam. Ex saepe quia. Voluptatem sed velit. Odio ut repellendus. Provident minus harum! Ut iste.'),
(37, 44, 45, 3, 'r', 13, 'Totam ut voluptatem. Aliquam voluptates atque. Omnis praesentium adipisci! Quia facilis voluptatem. Voluptatem atque numquam. Iste aperiam recusandae. Nam reprehenderit tempora. Veniam natus et. Quis ea.'),
(38, 40, 4, 13, 'd', 16, 'Voluptas et perspiciatis. Non quod recusandae! Quia doloremque mollitia. Eveniet officia dignissimos! Recusandae hic natus. Omnis vitae consectetur. Numquam omnis iste; nihil deleniti debitis. Esse.\r\n'),
(39, 6, 31, 16, 'u', 14, 'Sed at ut inventore et incidunt. Enim voluptas deleniti fuga. Ea rem obcaecati qui velit. Aliquid magnam commodi natus voluptatem minima inventore voluptatem modi perferendis!\r\n'),
(40, 34, 20, 20, 'q', 20, 'Repellat asperiores voluptatem quod voluptas a quia aut, consequatur quis. Voluptatem eveniet consectetur illum sint dolor et natus aperiam minima.'),
(41, 25, 44, 20, 'a', 15, 'Illum nisi est. Nisi illum velit! Rem maxime optio. Sed minus sit. Facere saepe iste? Repellendus quis ut. Ratione error nihil. Delectus unde voluptas. Laborum animi dolores.\r\nQuia non quae. Dolor!\r\n'),
(42, 14, 1, 12, 'g', 10, 'Dolore qui aut. Ex voluptatibus tempore? Facilis quia non! Ea consequatur ut.\r\nObcaecati veritatis dignissimos. Natus quas aut! Perspiciatis ea veniam. Officiis ut et. Vel cupiditate eveniet? At.\r\n'),
(43, 9, 27, 17, 'l', 16, 'Molestiae beatae quas. Quia accusantium necessitatibus; soluta consectetur est. Minima eos est; iste est fugit.\r\nAperiam at velit. Labore maiores cumque! Est et molestiae. Delectus voluptas animi.\r\n'),
(44, 50, 40, 0, 'p', 17, 'Natus ab alias.\r\nAliquam hic enim eius quibusdam non saepe quae excepturi unde. Et ut deserunt...\r\nEaque dolorum qui. Et id vel. A dignissimos corporis.'),
(45, 3, 37, 18, 'c', 20, 'Quo repudiandae labore. Ut dolores quis! Repellendus voluptatibus sequi. Non sed quas! Illo unde et; ut illum natus? Culpa explicabo minus! Tempore earum vel. Optio consequuntur quasi...\r\nA ab quis;'),
(46, 35, 31, 13, 'b', 18, 'Incidunt velit quidem. Neque tempore eos. Enim quidem sed? Dolor doloribus illum. Accusantium beatae nulla! Doloribus non nulla. Perspiciatis vitae autem. Sit voluptatum ut. Ratione impedit quia!'),
(47, 47, 25, 14, 'x', 15, 'Ipsa minus ipsa. Enim ipsum ut...\r\nEnim tenetur doloribus. Enim fugit quia. Ut nulla suscipit? Magni sit itaque! Unde rerum excepturi. Maxime blanditiis doloribus! Aut quaerat placeat. Cumque vitae nulla;'),
(48, 47, 14, 7, 'd', 19, 'Saepe modi et. Corporis ipsa aut. Qui et natus! Fugit nam dolore.\r\nLabore omnis quia. Error et nam. Reiciendis natus tempora. Nostrum aut omnis.\r\nAut fugit dolorem. Nam delectus illo.'),
(49, 8, 36, 18, 'x', 15, 'Enim aperiam in error temporibus. Ut quis veritatis beatae omnis voluptatum sed.\r\nCulpa debitis unde. Optio eligendi sequi? Modi doloribus nam; et inventore quis. Sint dolorum voluptas.'),
(50, 25, 12, 8, 'u', 15, 'Ut libero aperiam. Aperiam voluptas voluptatem. Incidunt voluptatibus molestias; error unde qui. Qui natus vitae. Voluptatem vel magni. Qui non incidunt. Quae nihil aut; magnam aut assumenda.\r\nCulpa.');

-- 
-- Dumping data for table gastosingresos
--
INSERT INTO gastosingresos VALUES
(1, '1975-02-19', 'Eaque commodi est repellat dolorem est rerum sit deserunt consequatur. Itaque error rerum! Rem laborum velit; omnis ipsam pariatur. Dolorem voluptatem possimus. Commodi eveniet fuga.', 232, 3, 7),
(2, '1987-04-08', 'Unde fugit maiores. Perspiciatis neque ullam! Voluptatem est aperiam. Qui ad nihil. Laborum quo qui. Dignissimos fugit magni; excepturi officia quam; in sunt impedit. Magnam aut cupiditate.', 226, 23, 28),
(3, '1984-01-06', 'Soluta fugit itaque. Fugit vel exercitationem; quia et aut. Sit expedita eligendi? Natus minus nobis; nesciunt unde hic. Animi odit consequatur. Sed cupiditate commodi. Sit dolor aut. Voluptatibus ut.', 145, 18, 27),
(4, '1994-09-27', 'Officia voluptatibus quia aut officiis. Ut et a rerum natus. Doloribus sit necessitatibus ipsam est qui iusto ducimus; qui vel ea architecto accusamus et impedit natus dolorem qui. Quia mollitia sunt.', 108, 42, 45),
(5, '2006-03-30', 'Quibusdam voluptatem earum. Fuga et vero consequatur magni, accusantium molestiae accusantium consequuntur fugiat. Et tempore in eum est repellendus saepe, sed laudantium sit!', 236, 20, 1),
(6, '2004-07-01', 'Consequatur dolorum debitis. Optio nobis voluptas impedit. Impedit eos voluptas dignissimos sapiente; qui libero corporis similique minus perspiciatis ipsa doloribus? Voluptatem ut ex. Est sit minus;', 118, 46, 28),
(7, '2001-09-03', 'Non suscipit aut exercitationem natus saepe ut assumenda vero labore.', 169, 30, 21),
(8, '1976-08-19', 'Unde vel iusto maiores. Delectus voluptatem molestias fugiat laudantium eligendi nulla? Nam et vel qui et natus earum, corrupti accusantium provident. Quibusdam placeat qui.', 224, 18, 23),
(9, '1971-06-17', 'Sit at illum sit ut molestiae ut nostrum cumque. Ratione quam nulla. Autem sit natus ad adipisci amet et aut dolor nostrum! Ut esse distinctio rem.', 202, 19, 42),
(10, '1970-10-21', 'Omnis deserunt magni eius excepturi sed perspiciatis esse hic. Amet unde libero unde autem ipsum; est qui rem veritatis repudiandae esse. Quidem aspernatur voluptatem? Quos magni quaerat!', 112, 47, 11),
(11, '2004-03-27', 'Delectus sequi eos. Vitae quis repudiandae optio enim qui dolores! Quia deleniti recusandae sed tempora pariatur quo praesentium alias qui; distinctio quaerat dolorem nostrum vel error esse expedita.', 251, 39, 4),
(12, '1984-05-19', 'Non temporibus dolorem sed. Dolor ullam tempore voluptatem doloremque molestias consequatur! Dignissimos assumenda numquam quia nobis. Est id obcaecati qui sit odit iste cum! Quaerat atque est; sapiente.', 159, 3, 37),
(13, '2002-07-26', 'Iste distinctio sit et. Ut ea non ut minima iusto omnis labore perspiciatis sit! Sed cum ex blanditiis fuga deleniti hic illum quasi quam.', 234, 27, 8),
(14, '1970-01-06', 'Maxime magnam aut commodi. Perferendis blanditiis non eos unde commodi ea nihil dicta! Nihil et sed. Dignissimos vitae nobis? Id culpa quisquam. Rerum pariatur voluptatem?', 249, 11, 21),
(15, '1990-02-06', 'Sunt velit placeat praesentium obcaecati tenetur natus quod corporis id.', 162, 10, 42),
(16, '1971-02-17', 'Voluptates sint magni deserunt. Non sed aspernatur facilis dolores; eos repellendus at. Dolor repellendus ut cupiditate impedit quisquam autem maiores sequi. Dolor temporibus est? Omnis et odit.', 282, 19, 3),
(17, '1971-05-27', 'Quia nesciunt quae quia vel sed reprehenderit consequuntur eos; vitae dolores pariatur sit autem. Quaerat fugit eius qui nostrum et corporis vel quam et...', 203, 2, 5),
(18, '1990-08-01', 'Omnis deserunt magnam architecto ut velit quis debitis et quasi. Consequatur quod debitis? Autem ex enim. Error iusto nostrum. Quisquam ut delectus! Sit voluptatem odit.', 144, 34, 24),
(19, '1999-02-23', 'Blanditiis quos dolor. Omnis ut enim sunt, autem eaque doloremque sint nesciunt alias! Rem omnis atque sed qui sed incidunt corporis perspiciatis eos.', 187, 29, 33),
(20, '2013-02-25', 'Veniam odio quia consectetur dolor nihil voluptates. Sapiente inventore fuga et odit sit! Illum modi voluptas consectetur dolorem, ut ab sit adipisci nostrum.', 182, 24, 33),
(21, '1977-04-20', 'Aperiam enim quibusdam sequi quas officiis sint. Voluptas incidunt sunt rerum qui sint quia voluptas quia! Ut suscipit alias? Unde consequuntur eligendi. Id voluptatem ut.', 230, 14, 4),
(22, '2014-06-27', 'Veritatis dolores placeat nihil cum a. Sapiente sunt beatae aperiam. Quisquam perspiciatis eos natus? Iure quia veniam. Est non molestias magni blanditiis. Deserunt odio in veniam quod possimus quia.', 100, 10, 32),
(23, '2009-10-16', 'Nam quasi architecto reiciendis adipisci eius natus consequatur dicta voluptatem.', 143, 33, 22),
(24, '1996-10-10', 'Quidem pariatur aut ut dolorem et qui eos. Officiis molestias iste. Totam omnis assumenda et inventore enim accusamus ut illum? Voluptatem beatae magnam. Perspiciatis quasi non!', 168, 12, 23),
(25, '2006-04-04', 'Error nesciunt qui. Perspiciatis voluptas tempore! Quia rem atque. Animi itaque ex. Consectetur aut cumque. Non velit id; qui voluptas eum. Pariatur officia incidunt! Accusantium excepturi aut. Ut fugit...', 102, 15, 47),
(26, '1990-04-23', 'Quibusdam veniam quia non et laudantium aut voluptatem ut. Vero neque placeat? Corporis ut impedit. Adipisci ut id. Sit qui perspiciatis? Sed quod corporis. Ipsum ut iste.', 299, 34, 13),
(27, '2003-06-06', 'A incidunt voluptatem perspiciatis. Quia enim vel ipsam consectetur eos voluptates facere sed; molestiae aut repellat iste non. Odit in sed. Sed non sit? Et voluptas aut...', 222, 32, 41),
(28, '2010-07-19', 'Et sed voluptatem perspiciatis vel corporis ut doloribus sed corporis. Aut repudiandae voluptatem et laboriosam officiis vitae vel, quia mollitia.', 249, 20, 40),
(29, '1970-01-04', 'Ut quia error inventore ut qui ut non et sit.', 292, 34, 37),
(30, '1997-01-06', 'Aut natus dignissimos explicabo excepturi et voluptatem. Quibusdam quia labore sed; vel aut inventore sit voluptas debitis perspiciatis nihil. Et ut excepturi et. Ea labore tenetur...', 286, 36, 47),
(31, '1999-07-13', 'Numquam reiciendis sed, id sit pariatur incidunt qui rerum quo.', 262, 5, 13),
(32, '1995-04-19', 'Sit recusandae ullam. Voluptatem quae aliquam. Asperiores est quia? Ad aut soluta. Explicabo ex magnam. Incidunt modi ab. Sunt non quia! Quas iusto ut. Minima repellendus mollitia!', 273, 16, 46),
(33, '1989-01-26', 'Quia tempora ut nemo et hic iste dicta, dolorem repudiandae. Doloribus natus cumque, voluptas consequatur laboriosam aut enim sequi ut.', 266, 8, 19),
(34, '1971-07-03', 'Odio veritatis voluptate; aspernatur sint voluptatem. Voluptatem exercitationem possimus repellat iste. Ut error molestiae voluptatem deleniti natus est sequi possimus. Suscipit voluptatem et vero et...', 274, 44, 28),
(35, '1986-01-10', 'Dolores sit tempore rerum laborum modi in nihil corporis cumque. Facere dolorum ab maiores delectus. Quia dolor praesentium dolores incidunt, dolorem hic iste quidem ut.', 162, 10, 19),
(36, '1995-11-19', 'Beatae quibusdam error rerum fugit error sunt harum laudantium tenetur.', 240, 22, 19),
(37, '1970-04-15', 'Sed est adipisci blanditiis modi dolorem officia. Quia libero rerum sequi et voluptate corrupti et nihil repellendus. Quaerat illo nisi rerum beatae temporibus quo dolor...', 245, 1, 4),
(38, '1998-12-08', 'Tenetur numquam voluptatem fuga omnis ad voluptas libero sed sit.', 226, 41, 20),
(39, '1980-01-25', 'Nesciunt sit obcaecati. Cum non unde. Excepturi dignissimos perspiciatis. Hic ratione placeat? Provident ipsam aut. Sed ipsa aut. Ipsa reiciendis cumque! Sed dolor enim. Ipsa aut illum. Ut sunt odit.', 256, 13, 43),
(40, '2003-12-12', 'Deleniti voluptatem assumenda inventore omnis possimus magnam aspernatur qui vitae. Quisquam laboriosam voluptas? Corporis ut officiis! Exercitationem repellat voluptatem; aut debitis iste. Quia est.', 110, 41, 1),
(41, '1990-06-10', 'Numquam molestiae eligendi soluta est tempore doloribus ut et laboriosam...', 230, 5, 20),
(42, '1998-12-11', 'Quia quo sequi. Qui et odit! Vero aut provident. Exercitationem quibusdam sit! Excepturi unde minus. Quam possimus fugiat. Maxime itaque tempore; qui voluptatem cumque. Voluptatibus odio natus!', 294, 30, 17),
(43, '1981-02-11', 'Et ut amet omnis. Suscipit vel omnis corrupti architecto adipisci ab dicta! Voluptate quam aut ad. Provident incidunt rerum fugit...', 286, 3, 12),
(44, '2018-05-12', 'Sint porro blanditiis et nemo eligendi nulla ut. Rerum voluptas eum. Eaque quo eveniet! Magni molestias fugit. Similique sed necessitatibus. Minima sit in. Ad modi officiis!', 224, 44, 15),
(45, '1973-12-16', 'Omnis sunt sit sunt aut dignissimos inventore delectus blanditiis aliquam.', 185, 4, 4),
(46, '1984-09-21', 'Vel nemo error aut eaque et laboriosam. Animi obcaecati consequuntur laboriosam qui laborum sit? Soluta pariatur quam. Consequatur nihil voluptas culpa exercitationem voluptatum rerum ut.', 225, 25, 47),
(47, '2017-04-06', 'Dolorem neque natus cupiditate explicabo velit voluptatum est ea. Sed vel iusto ad et quaerat laborum eligendi necessitatibus quaerat. Ut vero nisi? Temporibus ea at.', 205, 34, 8),
(48, '1970-02-13', 'Non architecto ea. Et ut et? Vitae beatae iure. Eveniet enim nobis. Hic corporis quam. Dolores fuga vel! Beatae et sunt. Ut voluptas in! Fugit rerum rem. Facilis sint aliquid!', 154, 36, 48),
(49, '1977-06-04', 'Perspiciatis nam amet tempore ducimus quia voluptatem dolore natus. Delectus deserunt necessitatibus. Sunt voluptatem atque nostrum quas. Id tempore doloribus. Ab similique voluptatem nulla ipsa!', 145, 43, 2),
(50, '1986-02-19', 'Natus quidem sit eveniet illo ullam asperiores animi ad. Officiis adipisci cumque unde? Ut enim aliquid magni ab et omnis eligendi et quo.', 271, 24, 17);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;