<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gastosingresos */

$this->title = 'Create Gastosingresos';
$this->params['breadcrumbs'][] = ['label' => 'Gastosingresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gastosingresos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
