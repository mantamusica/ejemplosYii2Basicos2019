<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gastosingresos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gastosingresos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Gastosingresos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fecha',
            'concepto',
            'importe',
            'comunidad',
            //'pisoLocal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
