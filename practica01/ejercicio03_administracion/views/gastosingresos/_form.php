<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Gastosingresos */
/* @var $form yii\widgets\ActiveForm */

$comunidad=\app\models\Comunidad::find()->all();
$listComunidades=ArrayHelper::map($comunidad,'id','direccion');
$pisolocal=\app\models\Pisolocal::find()->all();
$listapisolocal=ArrayHelper::map($pisolocal,'id', 'id');

?>

<div class="gastosingresos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'concepto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'importe')->textInput() ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listComunidades,
        ['prompt'=>'Selecciona una.']
    ) ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listapisolocal,
        ['prompt'=>'Selecciona una.']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
