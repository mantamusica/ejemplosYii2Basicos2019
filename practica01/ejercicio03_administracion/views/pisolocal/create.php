<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pisolocal */

$this->title = 'Create Pisolocal';
$this->params['breadcrumbs'][] = ['label' => 'Pisolocals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pisolocal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
