<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Pisolocal */
/* @var $form yii\widgets\ActiveForm */

$comunidad=\app\models\Comunidad::find()->all();
$listComunidades=ArrayHelper::map($comunidad,'id','direccion');
$propietario=\app\models\Propietario::find()->all();
$listpropietarios=ArrayHelper::map($propietario,'id','nombre');


?>

<div class="pisolocal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listComunidades,
        ['prompt'=>'Selecciona una.']
    ) ?>

    <?= $form->field($model, 'id')->dropDownList(
        $listpropietarios,
        ['prompt'=>'Selecciona una.']
    ) ?>

    <?= $form->field($model, 'planta')->textInput() ?>

    <?= $form->field($model, 'letra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coeficiente')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
