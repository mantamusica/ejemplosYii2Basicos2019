<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <div class="jumbotron"><h1><?=$datos?></h1></div>

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <p>Listar todos los empleados.</p>
            <?= Html::a('Consulta', ['/emple/consultas_1'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Listar el deptno y apellidos del empleado.</p>
            <?= Html::a('Consulta', ['/emple/consultas_2'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Listar empleados dept_no 20 o 30.</p>
            <?= Html::a('Consulta', ['/emple/consultas_3'], ['class'=>'btn btn-primary']) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <p>Listar empleados dept_no 20  0 30 y salario > 1500.</p>
            <?= Html::a('Consulta', ['/emple/consultas_4'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Listar los dept_no de todos los empleados cuyo salario sea > 1500 o apellidos comience por S.</p>
            <?= Html::a('Consulta', ['/emple/consultas_5_6','id' => 0], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Nombre de los dept_no que cumplen la consulta 5.</p>
            <?= Html::a('Consulta', ['/emple/consultas_5_6','id' => 1], ['class'=>'btn btn-primary']) ?>
        </div>
    </div>
<!--    <hr>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <p>Listar empleados dept_no 20  0 30 y salario > 1500.</p>
            <?/*= Html::a('Ver', ['/emple/consultas_4'], ['class'=>'btn btn-primary']) */?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Listar los dept_no 10 de todos los empleados cuyo salario sea > 1500 o apellidos comience por S.</p>
            <?/*= Html::a('Ver', ['/emple/consultas_5'], ['class'=>'btn btn-primary']) */?>
        </div>
        <div class="col-sm-6 col-md-4">
            <p>Nombre de los dept_no que cumplen la consulta 5.</p>
            <?/*= Html::a('Ver', ['/emple/consultas_6'], ['class'=>'btn btn-primary']) */?>
        </div>
    </div>-->

</div>
