<?php

namespace app\controllers;

use app\models\Depart;
use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),

        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAgrupar()
    {
        return $this->render('eleccion', [
            'datos' => 'Consultas de Selección de Empleados.'
        ]);

    }

    public function actionConsultas_1()
    {
        $datos = Emple::find()->all();

        return $this->render('seleccion', [
            'datos' => $datos,
            'consulta'=>'Consulta 1 - ',
            'texto'=>'Listado de todos Empleados',


        ]);
    }

    public function actionConsultas_2()
    {
        $datos = Emple::find()->select('dept_no, apellido')->all();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 2 - ',
            'texto'=>'Listado el Departamento y el apellido del empleado',

        ]);
    }

    public function actionConsultas_3()
    {
        $num1 = 20;
        $num2 = 30;
        $datos = Emple::find()
            ->where('dept_no = :dept_no', [':dept_no' => $num1])
            ->orWhere('dept_no = :dept_no', [':dept_no' => $num2])
            ->all();


        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 3 - ',
            'texto'=>'Listar empleados dept_no 20  0 30',

        ]);
    }

    public function actionConsultas_4()
    {

        $datos = Emple::find()
            ->where(['and', 'salario>1500', ['or', 'dept_no=20', 'dept_no=30']])
            ->all();


        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 4 - ',
            'texto'=>'Listar empleados dept_no 20  0 30 y salario > 1500',

        ]);
    }

/*    public function actionConsultas_5()
    {

        $datos = Emple::find()
            ->select('dept_no')
            ->where(['and', 'dept_no>10', ['or', 'salario>1500', 'apellido="S%"']])
            ->all();


        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 5 - ',
            'texto'=>'Listar los dept_no 10 de todos los empleados cuyo salario sea > 1500 o apellidos comience por S',

        ]);
    }*/

/*    public function actionConsultas_6()
    {

        $datos = Emple::find()
            ->where(['and', 'dept_no>10', ['or', 'salario>1500', 'apellido="S%"']])
            ->all();

        foreach ($datos as $item) {
            $departamentos[] = $item ['dept_no'];
        }

        $resultado = array_unique($departamentos);

        $datos2 = Depart::find()
            ->select('dnombre')
            ->where(['not in', 'dept_no', $resultado])
            ->all();


        return $this->render('seleccion', [

            'datos' => $datos2,
            'consulta'=>'Consulta 6 - ',
            'texto'=>'Nombre de los dept_no que cumplen la consulta 5',

        ]);
    }*/

    public function actionConsultas_5_6($id)
    {
        $datos = Emple::find()
            ->select('dept_no')
            ->where(['or', 'salario > 1500', ['like', 'apellido', 'S%']])
            ->all();

        if ($id === '0') {

            $consulta = 'Consulta 5 - ';
            $texto = 'Listar los dept_no 10 de todos los empleados cuyo salario sea > 1500 o apellidos comience por S';

        } else {

            foreach ($datos as $item) {
                $departamentos[] = $item ['dept_no'];
            }

            $resultado = array_unique($departamentos);

            $datos = Depart::find()
                ->select('dnombre')
                ->where(['not in', 'dept_no', $resultado])
                ->all();

            $consulta = 'Consulta 6 - ';
            $texto = 'Nombre de los dept_no que no cumplen la consulta 5';

        }

        return $this->render('seleccion', [
            'datos' => $datos,
            'consulta' => $consulta,
            'texto' => $texto]);
    }


}
