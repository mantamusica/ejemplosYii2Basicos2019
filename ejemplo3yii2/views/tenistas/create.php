<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tenista */

$this->title = 'Agregar Tenista';
$this->params['breadcrumbs'][] = ['label' => 'Tenistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenista-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
