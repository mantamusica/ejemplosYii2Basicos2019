<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tenista */

?>
<div class="tenistas-view">
    <div class="jumbotron"><h1><?=$datos?></h1></div>

    <div class="row">
        <div class="col-md-4">
            <p>Tenista con el id número 2.</p>
            <?= Html::a('Ver', ['/tenistas/consultas_model_1'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-md-4">
            <p>Listado de tenistas ordenados por nombre.</p>
            <?= Html::a('Ver', ['/tenistas/consultas_model_1'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="col-md-4">
            <p>Listado de tenistas por id y nombre.</p>
            <?= Html::a('Ver', ['/tenistas/consultas_model_4'], ['class'=>'btn btn-primary']) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <p>Listado de tenistas que empiezan por N o su edad es mayor de 40.</p>
            <?= Html::a('Ver', ['/tenistas/consultas_model_4'], ['class'=>'btn btn-primary']) ?>
        </div>
    </div>
</div>
