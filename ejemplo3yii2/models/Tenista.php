<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenista".
 *
 * @property int $id
 * @property string $nombre
 * @property string $correo
 * @property string $fechaNacimiento
 * @property int $altura
 * @property string $peso
 * @property string $fechaBaja
 * @property int $activo
 * @property int $nacion
 *
 * @property Naciones $nacion0
 */
class Tenista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento', 'fechaBaja'], 'safe'],
            [['altura', 'activo', 'nacion'], 'integer'],
            [['nombre', 'correo', 'peso'], 'string', 'max' => 255],
            [['correo'], 'email'],
            [['nacion'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['nacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'activo' => 'Activo',
            'fechaBaja' => 'Fecha Baja',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'altura' => 'Altura(cm)',
            'peso' => 'Peso(kg)',
            'nacion' => 'Nacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNacion0()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'nacion']);
    }
}
