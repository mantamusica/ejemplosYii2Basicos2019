<?php

namespace app\controllers;

use Yii;
use app\models\Tenista;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TenistasController implements the CRUD actions for Tenista model.
 */
class TenistasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tenista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Tenista::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tenista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tenista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tenista();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionTodos()
    {
        $consulta = Tenista::find()->all();

        $consultaArray = Tenista::find()->asArray()->all();

        return $this->render('todos', [
            'datos' => $consulta,
            'datos1' => $consultaArray
        ]);
    }

    public function actionAgrupar()
    {
        return $this->render('eleccion', [
            'datos' => 'Consultas de Selección de Tenistas.'
        ]);

    }

    public function actionConsultas_sql_0()
    {
        $conexion = Yii::$app->db;

        $datos = $conexion->createCommand("Select * from tenista")->queryAll();

        return $this->render('seleccion', [
            'datos' => $datos,
            'consulta'=>'Consulta 1 - ',
            'texto'=>'Listado de todos Tenistas',

        ]);
    }
    public function actionConsultas_sql_1()
    {
        $conexion = Yii::$app->db;

        $datos = $conexion->createCommand("Select * from tenista where id = 2")->queryAll();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 2 - ',
            'texto'=>'Listado de todos Tenistas con id = 2'

        ]);
    }
    public function actionConsultas_sql_2()
    {
        $conexion = Yii::$app->db;

        $datos = $conexion->createCommand("Select * from tenista order by nombre")->queryAll();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 3 - ',
            'texto'=>'Listado de todos Tenistas ordenados por nombre',


        ]);
    }
    public function actionConsultas_sql_3()
    {
        $conexion = Yii::$app->db;

        $datos = $conexion->createCommand("Select id, nombre from tenista ")->queryAll();


        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 4 - ',
            'texto'=>'Listado de todos Tenistas mostrando id y nombre',


        ]);
    }
    public function actionConsultas_sql_4()
    {
        $conexion = Yii::$app->db;

        $datos = $conexion->createCommand("Select * from tenista where nombre = 'N%' or  edad < 40")->queryAll();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 5 - ',
            'texto'=>'Listado de todos Tenistas mayores de 40 o que empiecen por N',

        ]);
    }
    public function actionConsultas_model_0()
    {
        $datos = Tenista::find()->all();

        return $this->render('seleccion', [
            'datos' => $datos,
            'consulta'=>'Consulta 1 - ',
            'texto'=>'Listado de todos Tenistas',


        ]);
    }
    public function actionConsultas_model_1()
    {

        $userid=2;
        $datos = Tenista::find()
            ->where('id = :userid', [':userid' => $userid])
            ->one();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 2 - ',
            'texto'=>'Listado de todos Tenistas con id = 2',

        ]);
    }
    public function actionConsultas_model_2()
    {

        $datos = Tenista::find()
            ->orderBy('nombre')
            ->all();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 3 - ',
            'texto'=>'Listado de todos Tenistas ordenados por nombre',

        ]);
    }
    public function actionConsultas_model_3()
    {

        $datos = Tenista::find()->select('id, nombre')->all();

        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 4 - ',
            'texto'=>'Listado de todos Tenistas mostrando id y nombre',

        ]);
    }
    public function actionConsultas_model_4()
    {

        $edad = 25;
        $letra = 'N%';
        $datos = Tenista::find()
//            ->where("nombre like 'n%' or edad<40");
            ->where('edad < :edad', [':edad' => $edad])
            ->orWhere('nombre = :nombre', [':nombre' => $letra])
            ->all();


        return $this->render('seleccion', [

            'datos' => $datos,
            'consulta'=>'Consulta 5 - ',
            'texto'=>'Listado de todos Tenistas mayores de 40 o que empiecen por N',

        ]);
    }

    /**
     * Updates an existing Tenista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tenista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tenista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tenista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = Tenista::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
