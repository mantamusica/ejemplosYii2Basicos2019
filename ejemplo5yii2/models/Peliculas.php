<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id
 * @property string $titulo
 * @property string $descripcion
 * @property string $cartel
 * @property int $duracion
 * @property int $year
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['descripcion'], 'string'],
            [['duracion', 'year'], 'integer'],
            [['titulo'], 'string', 'max' => 50],
            [['cartel'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            //'cartel' => 'Cartel',
            'duracion' => 'Duracion',
            'year' => 'Year',
            'cartel' => 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PeliculasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PeliculasQuery(get_called_class());
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->cartel->saveAs('imgs/' . $this->cartel->baseName . '.' . $this->cartel->extension);
            return true;
        } else {
            return false;
        }
    }
}
