<?php

use yii\db\Migration;

/**
 * Class m190327_190013_crearPelicula
 */
class m190327_190013_crearPelicula extends Migration
{
    /**
     * {@inheritdoc}
     */
    
    private $a=[
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porttitor leo in nibh facilisis, at dapibus ipsum faucibus. Etiam ultrices arcu ante, sit amet luctus nibh varius a. Ut velit turpis, vulputate sit amet urna et, imperdiet dignissim dui. Nam dapibus consectetur turpis, id sollicitudin tellus suscipit non. Donec posuere neque ut pulvinar laoreet. Duis ac eros congue, tincidunt neque at, ultrices augue. Quisque tristique sagittis faucibus. Fusce in metus ultricies, mollis diam ut, malesuada tellus. In vel risus varius, tempor ipsum id, dictum neque. Morbi ligula tortor, eleifend non pellentesque a, tincidunt at massa.

Duis sed dignissim turpis, ut tincidunt eros. Sed eu scelerisque dui, vestibulum volutpat sapien. Nulla tristique mauris ex, in pretium arcu aliquet congue. Curabitur scelerisque hendrerit turpis, ac placerat magna bibendum in. Ut luctus at metus id feugiat. In aliquam mi vitae felis vehicula, in fringilla lorem cursus. Ut tristique augue eget odio ultricies laoreet. Vestibulum congue sit amet ipsum in tincidunt. Aliquam egestas pellentesque lacus, ut tempus ex elementum vitae. Suspendisse aliquam augue sit amet metus malesuada maximus. Phasellus vestibulum turpis ut diam vestibulum maximus.",
        "In risus libero, consectetur in neque ac, hendrerit aliquet est. Nam sed facilisis turpis, vestibulum porttitor tellus. Fusce eget lacus egestas, placerat elit nec, commodo neque. Cras dapibus erat quis ipsum feugiat venenatis. Nullam vel molestie arcu, ac suscipit lorem. Pellentesque aliquam euismod elit, vitae malesuada tortor molestie iaculis. Cras in tortor in nulla egestas luctus. Donec suscipit quis est eget mollis. Integer pulvinar lorem sed semper aliquet. Aenean neque lectus, facilisis eu mauris nec, ultrices blandit ex. Etiam finibus, quam vitae malesuada viverra, neque diam ultricies magna, at malesuada libero lacus nec leo. Maecenas rhoncus eros ac hendrerit placerat. In sed porttitor lorem, ut viverra quam. Sed eget sodales lectus, non varius metus.",
        "Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia. Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte. Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario, cuando recostado sobre la crecida hierba, cerca de la cascada, mi vista, más próxima a la tierra, descubre multitud de menudas y diversas plantas; cuando siento más cerca de mi corazón los rumores de vida de ese pequeño mundo que palpita en los tallos de las hojas, y veo las formas innumerables e infinitas de los gusanillos y de los insectos; cuando siento, en fin, la presencia del Todopoderoso, que nos ha creado",
        "Quiere la boca exhausta vid, kiwi, piña y fugaz jamón. Fabio me exige, sin tapujos, que añada cerveza al whisky. Jovencillo emponzoñado de whisky, ¡qué figurota exhibes! La cigüeña tocaba cada vez mejor el saxofón y el búho pedía kiwi y queso. El jefe buscó el éxtasis en un imprevisto baño de whisky y gozó como un duque. Exhíbanse politiquillos zafios, con orejas kilométricas y uñas de gavilán. El cadáver de Wamba, rey godo de España, fue exhumado y trasladado en una caja de zinc que pesó un kilo. El pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y frío, añoraba a su querido cachorro. El veloz murciélago hindú comía feliz cardillo y kiwi. La cigüeña tocaba el saxofón detrás del palenque de paja.Quiere la boca exhausta vid, kiwi, piña y fugaz jamón. Fabio me exige, sin tapujos, que añada cerveza al whisky. Jovencillo emponzoñado de whisky, ¡qué figurota exhibes! La cigüeña tocaba cada vez mejor el saxofón y el búho pedía kiwi y queso. El jefe buscó el éxtasis en un imprevisto baño de whisky y gozó como un duque. Exhíbanse politiquillos zafios, con orejas kilométricas y uñas de gavilán. El cadáver de Wamba, rey godo de España, fue",
        "Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual. Sobre la mesa había desparramado un muestrario de paños - Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado. La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía. «Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de",
        "Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro. Todas estas borrascas que nos suceden son señales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquí se sigue que, habiendo durado mucho el mal, el bien está ya cerca. Así que, no debes congojarte por las desgracias que a mí me suceden, pues a ti no te cabe parte dellas.Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro. Todas estas borrascas que nos suceden son señales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquí se sigue que, habiendo durado mucho el mal, el bien está ya cerca. Así que, no debes congojarte por las desgracias que a mí me suceden, pues a ti no",
        "Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su",
        "Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia. Estoy solo, y me felicito de vivir en este país, el más a propósito para."
        ];
    
    private function texto(){
        $longitud= count($this->a);
        $posicion= random_int(0, $longitud);
        return $this->a[$posicion];
    }
    
    public function safeUp()
    {
        $this->createTable("peliculas", [
           'id'=> $this->primaryKey(),
            'titulo'=>$this->string(50)->notNull(),
            'descripcion'=>$this->text(),
            'cartel'=>$this->string(30),
            'duracion'=>$this->integer(),
            'year'=>$this->integer(),
        ]);
        
        $this->insert("peliculas", [
            'titulo'=>'Pelicula1',
            'descripcion'=>$this->texto(),
            'cartel'=>"cartel1",
            "duracion"=>90,
            "year"=>1999,
            ]);
        
        $this->insert("peliculas",[ 
            'titulo'=>'Pelicula2',
            'descripcion'=>$this->texto(),
            'cartel'=>"cartel2",
            "duracion"=>92,
            "year"=>2010,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
        $this->dropTable('peliculas');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190327_190013_crearPelicula cannot be reverted.\n";

        return false;
    }
    */
}
